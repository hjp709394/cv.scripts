import cStringIO
import imghdr
import urllib2
import sys
import re
import traceback
import numpy as np
import argparse
from collections import defaultdict
from PIL import Image
import cv2
from matplotlib import pyplot as plt
import math
from datetime import datetime
import urllib
import json
from os import path
import os

def url_to_image(url):
    # download the image, convert it to a NumPy array, and then read
    # it into OpenCV format
    resp = urllib.urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    # return the image
    return image

def show_img(img):
    plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB));
    plt.show();

def makeparrentdir(pathstr):
    dirstr = path.dirname(pathstr)
    if not path.exists(dirstr):
        os.makedirs(dirstr)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('command', type=str, help='rec | poly | label')
    parser.add_argument('--nj', type=int, default=1)
    parser.add_argument('--nthj', type=int, default=0)
    #parser.add_argument('--json', type=str, required=True)
    parser.add_argument('--jsonlist', type=str, required=True)
    parser.add_argument('--indir', type=str, required=True)
    parser.add_argument('--outdir', type=str, required=True)
    args = parser.parse_args()

    suffix_pat = re.compile('\.[^\.]*$')

    with open(args.jsonlist) as jsonlist:
        for idx, line in enumerate(jsonlist):
            if idx % args.nj != args.nthj:
                continue
            line = line.strip().rstrip()
            with open(line) as jsonfile:
                try:
                    jsondata = json.load(jsonfile)
                    image_path = jsondata['ImagePath']
                    out_path = suffix_pat.sub('.png', image_path)
                    if args.command == 'rec' or args.command == 'poly':
            	        img = cv2.imread(args.indir + image_path)
                    elif args.command == 'label':
            	        img = cv2.imread(args.indir + image_path, cv2.CV_LOAD_IMAGE_GRAYSCALE)
                    if args.command == 'label':
                        img = np.zeros((img.shape[0], img.shape[1], 1), dtype=np.uint8)

                    print args.indir + image_path
            
                    if args.command == 'poly':
                        for poly in jsondata['Polys']:
                            poly = tuple([tuple([pp for pp in p]) for p in poly])
                            poly = np.int0(poly)
                            cv2.drawContours(img, [poly], 0, (0,255,0), 2)
                    else:
                        for rec in jsondata['Rects']:
                            x = rec['x']
                            y = rec['y']
                            w = rec['width']
                            h = rec['height']
                            if args.command == 'rec':
                                cv2.rectangle(img, (int(x),int(y)), (int(x+w), int(y+h)), (0,255,0), 3)
                            elif args.command == 'label':
                                cv2.rectangle(img, (int(x),int(y)), (int(x+w), int(y+h)), (1), -1)

                    makeparrentdir(args.outdir + out_path)
                    cv2.imwrite(args.outdir + out_path, img)
                    print args.indir + image_path

                except (KeyboardInterrupt, SystemExit):
                    raise
                except:
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
                    sys.stdout.write("[error]: while dealing with record: %s\n" % (line))
                    for info in sys.exc_info():
                        sys.stdout.write("[error]: Unexpected error: %s\n" % (info))
 

   

#     with open(args.list_file, 'r') as listf:
#         for idx, line in enumerate(listf):
#             line = line.strip().rstrip()
#             if idx % args.nj != args.nthj:
#                 continue
# 
#             fields = line.strip().rstrip().split()
#             file = fields[0]
# 
#             try:
#                 if file.startswith( 'http' ):
#                     response = urllib2.urlopen(file)
#                     data = cStringIO.StringIO(response.read())
#                 else:
#                     with open(file, 'r') as f:
#                         data = cStringIO.StringIO(f.read())
#                 print '[info] ', file, imghdr.what(data)
#             except:
#                 print '[error] ', file
# 

