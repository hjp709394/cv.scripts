#import sys
import caffe
#import matplotlib
import numpy as np
import lmdb
import argparse
#from collections import defaultdict
import cv2
#import pylab as pl
#import os

def rgb_heat(minimum, maximum, value):
    minimum, maximum = float(minimum), float(maximum)
    ratio = 2 * (value-minimum) / (maximum - minimum)
    b = (np.maximum(0, 255*(1 - ratio))).astype(np.uint8)
    r = (np.maximum(0, 255*(ratio - 1))).astype(np.uint8)
    g = 255 - b - r
    return r, g, b

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="draw segmentation result according to caffe feature stored into an lmdb")
    parser.add_argument('--lmdb', type=str, required=True)
    parser.add_argument('--flist', type=str, required=True)
    args = parser.parse_args()

    lmdb_env = lmdb.open(args.lmdb)
    lmdb_txn = lmdb_env.begin()
    lmdb_cursor = lmdb_txn.cursor()

    fl = []
    with open(args.flist) as ifile:
        for line in ifile:
            fl.append(line.strip().rstrip().split(' '))

    idx = 0
    for key, value in lmdb_cursor:
        datum = caffe.proto.caffe_pb2.Datum()
        datum.ParseFromString(value)
        label = int(datum.label)
        prob = caffe.io.datum_to_array(datum)
        p = np.exp(prob[1]) / (np.exp(prob[0]) + np.exp(prob[1]))
        img = cv2.imread( fl[idx][0] )
        
        print key, label, p.shape, img.shape
        heat_img = np.zeros( img.shape )
        R,G,B = rgb_heat(0, 1, p)
        height = min(img.shape[0], p.shape[0])
        width = min(img.shape[1], p.shape[1])
        heat_img = img[0:height,0:width,:] * 0.2 + np.asarray([B,G,R]).transpose((1,2,0))[0:height,0:width,:] * 0.8
#        for row in range(0, img.shape[0]):
#            if row >= p.shape[0]:
#                break;
#            for col in range(0, img.shape[1]):
#                if col >= p.shape[1]:
#                    break
#                r, g, b = rgb_heat(0, 1, p[row,col])
#                heat_img[row,col,:] = img[row, col, :] * 0.2 + np.asarray([b, g, r]) * 0.8
        cv2.imwrite(fl[idx][1], heat_img)
        idx = idx + 1
