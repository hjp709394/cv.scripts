import cv2
import sys
import traceback
import caffe
import numpy as np
import argparse
from collections import defaultdict
from PIL import Image
from matplotlib import pyplot as plt
import math
from datetime import datetime
import urllib
from pprint import pprint
import json
from shapely.geometry import Polygon
from shapely.geos import TopologicalError
from os import path
import os
import distutils.util

def makeparrentdir(pathstr):
    dirstr = path.dirname(pathstr)
    if not path.exists(dirstr):
        os.makedirs(dirstr)


def rgb_heat(minimum, maximum, value):
    minimum, maximum = float(minimum), float(maximum)
    ratio = 2 * (value-minimum) / (maximum - minimum)
    b = (np.maximum(0, 255*(1 - ratio))).astype(np.uint8)
    r = (np.maximum(0, 255*(ratio - 1))).astype(np.uint8)
    g = 255 - b - r
    return r, g, b

def url_to_image(url):
    # download the image, convert it to a NumPy array, and then read
    # it into OpenCV format
    resp = urllib.urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    return image

def show_img(img):
    plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB));
    plt.show();

def cal_prob(net, image, blob_names):
    image[:,:,0] = image[:,:,0] - args.mean0
    image[:,:,1] = image[:,:,1] - args.mean1
    image[:,:,2] = image[:,:,2] - args.mean2

    ### in opencv: height X width X channel -> in caffe: channel X height X width
    image = np.rollaxis(image, 2)

    input_shape = [ net.blobs[net.inputs[0]].shape[i] for i in range(0, len(net.blobs[ net.inputs[0] ].shape)) ]
    if image.shape[1] != input_shape[2] or image.shape[2] != input_shape[3]:
        net.blobs[net.inputs[0]].reshape( 1, image.shape[0], image.shape[1], image.shape[2] )

    out = net.forward_all(data=np.asarray([image]))
    ps = []
    for bn in blob_names:
        prob = np.asarray(out[bn])
        p = np.exp(prob[0,1,:,:]) / (np.exp(prob[0,0,:,:]) + np.exp(prob[0,1,:,:]))
        ps.append(p)
    
    return ps

def remove_dup(boxs, recs, keep):
    try:
        for bidx, b in enumerate(boxs):
            if keep[bidx] == 0:
                continue
            poly = Polygon([(x,y) for x,y in b])
            for bidx2 in range(bidx + 1, len(boxs)):
                if keep[bidx2] == 0:
                    continue
                poly2 = Polygon([(x,y) for x,y in boxs[bidx2]])
                intarea = poly.intersection(poly2).area 
                if intarea / (poly.area + poly2.area - intarea) > 0.5:
                    keep[bidx2] = 0

#        popidx = [1 for i in range(len(boxs))]
#        for bidx, b in enumerate(boxs):
#            if popidx[bidx] == 0:
#                continue
#            for bidx2 in range(bidx + 1, len(boxs)):
#                if popidx[bidx2] == 0:
#                    continue
#                if np.max(np.abs(np.asarray(boxs[bidx]) - np.asarray(boxs[bidx2]))) < 1.:
#                    if keep[bidx] == 0 and keep[bidx2] == 1:
#                        keep[bidx] = 1
#                        keep[bidx2] = 0
#                    popidx[bidx2] = 0
#        for pidx in range(len(boxs) - 1, -1, -1):
#            if popidx[pidx] == 0:
#                keep.pop(pidx)
#                boxs.pop(pidx)
#                recs.pop(pidx)

    except (KeyboardInterrupt, SystemExit):
        raise
    except:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        for info in sys.exc_info():
          sys.stdout.write("[error]: Unexpected error: %s\n" % (info))


#def split_top(p, boxs, recs, keep, contourArea):
#    lastIdx = len(boxs) - 1
#    if lastIdx == -1:
#        return
#    x0, y0, w0, h0 = recs[lastIdx]
#    subp = p[y0:y0+h0,x0:x0+w0]
#    maxprob = np.max(subp)
#    if maxprob < 0.9:
#        return
#    bin_img = np.zeros( subp.shape )
#    bin_img[ subp > maxprob - 0.2 ] = 1
#    bin_img = bin_img.astype(np.uint8)
#    contours, hierarchy = cv2.findContours(bin_img,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
#    conArea = 0
#    for con in contours:
#        conArea += cv2.contourArea(con)
#    if conArea / contourArea < 0.7: # 0.7
#        return
#
#    keep[lastIdx] = 0
#    for con in contours:
#        conArea = cv2.contourArea(con)
#
#        x,y,w,h = cv2.boundingRect(con)
#
#        rect = cv2.minAreaRect(con)
#        box = cv2.cv.BoxPoints(rect)
#
#        if conArea < 10 or box[0] == box[1] or box[1] == box[2] or box[2] == box[3] or box[3] == box[0]:
#            continue
#
#        recs.append([x+x0,y+y0,w,h])
#        boxs.append(box + np.asarray([x0, y0]))
#
#        if conArea < 200:
#            keep.append(0)
#        else:
#            keep.append(1)


#def split_top(p, boxs, recs, keep, contourArea):
#    lastIdx = len(boxs) - 1
#    if lastIdx == -1:
#        return
#    x0, y0, w0, h0 = recs[lastIdx]
#    subp = p[y0:y0+h0,x0:x0+w0]
#    maxprob = np.max(subp)
#    if maxprob < 0.7:
#        return
#    step = min(1 - maxprob, 0.025)
#
#    contours = []
#    for stepcnt in range(10,0,-1):
#        prob = maxprob - step * stepcnt
#        if prob < 0.6:
#            continue
#
#        bin_img = np.zeros( subp.shape )
#        bin_img[ subp > prob ] = 1
#        bin_img = bin_img.astype(np.uint8)
#        cont, hier = cv2.findContours(bin_img,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
#        if len(cont) > 1 and len(cont) > len(contours):
#            contours = cont
#
#            conArea = 0
#            for con in contours:
#                conArea += cv2.contourArea(con)
#        #    if conArea / contourArea < 0.7: # 0.7
#        #        return
#
#            for con in contours:
#                cur_conArea = cv2.contourArea(con)
#
#                x,y,w,h = cv2.boundingRect(con)
#                recs.append([x+x0,y+y0,w,h])
#
#                rect = cv2.minAreaRect(con)
#                box = cv2.cv.BoxPoints(rect)
#                boxs.append(box + np.asarray([x0, y0]))
#
#                if cur_conArea < 200:
#                    keep.append(0)
#                else:
#                    keep.append(1)
#
#    keep[lastIdx] = 0

def split_top(p, boxs, recs, keep, contourArea):
    lastIdx = len(boxs) - 1
    if lastIdx == -1:
        return
    x0, y0, w0, h0 = recs[lastIdx]
    subp = p[y0:y0+h0,x0:x0+w0]
    maxprob = np.max(subp)
    if maxprob < 0.7:
        return

    contours = []
    prob = 0.1
    step = 0.1
    while prob < maxprob and step > 0.001:
        step = (1 - prob) / 20
        prob = prob + step

        bin_img = np.zeros( subp.shape )
        bin_img[ subp > prob ] = 1
        bin_img = bin_img.astype(np.uint8)
        cont, hier = cv2.findContours(bin_img,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        if len(cont) > 0 and len(cont) != len(contours):
            contours = cont

            conArea = 0
            for con in contours:
                conArea += cv2.contourArea(con)

            for con in contours:
                cur_conArea = cv2.contourArea(con)

                x,y,w,h = cv2.boundingRect(con)
                recs.append([x+x0,y+y0,w,h])

                rect = cv2.minAreaRect(con)
                box = cv2.cv.BoxPoints(rect)
                boxs.append(box + np.asarray([x0, y0]))

                if cur_conArea < 200:
                    keep.append(0)
                else:
                    keep.append(1)

    keep[lastIdx] = 0


def obtain_box(ps, thresh=0.5, thresh2=0.7, prev_recs=None, do_split=False):
    try:
#    if True:
#        kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(5, 5))
#        p = cv2.morphologyEx(p, cv2.MORPH_CLOSE, kernel, 5)
#        p =  cv2.morphologyEx(p, cv2.MORPH_OPEN, kernel, 5)

        if prev_recs == None:
            prev_recs = [{'x': 0, 'y': 0, 'width': ps[0].shape[1], 'height': ps[0].shape[0] }]
        boxs = []
        recs = []
        keep = []
        for curp in ps:
            for prev_r in prev_recs:
                x0, y0, width0, height0 = prev_r['x'], prev_r['y'], prev_r['width'], prev_r['height']
                p = curp[y0:y0+height0, x0:x0+width0]

                bin_img = np.zeros( p.shape )
                bin_img[ p > thresh ] = 1
                bin_img = bin_img.astype(np.uint8)
                contours, hierarchy = cv2.findContours(bin_img,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

                bin_img2 = np.zeros( p.shape )
                bin_img2[ p > thresh2 ] = 1
                bin_img2 = bin_img2.astype(np.uint8)

    #            bin_img3 = np.zeros( p.shape )
    #            bin_img3[ p > 0.8 ] = 1
    #            bin_img3 = bin_img3.astype(np.uint8)

                for con in contours:
                    x,y,w,h = cv2.boundingRect(con)
                    rect = cv2.minAreaRect(con)
                    box = cv2.cv.BoxPoints(rect)

                    conArea = cv2.contourArea(con)
                    highpArea = np.sum(bin_img2[y:y+h,x:x+w])
                    recArea = w * h
                    
                    if conArea < 10 or box[0] == box[1] or box[1] == box[2] or box[2] == box[3] or box[3] == box[0]:
                        continue

                    boxs.append(box + np.asarray([x0, y0]))
                    recs.append([x+x0,y+y0,w,h])

    #                highpArea3 = np.sum(bin_img3[y:y+h,x:x+w])

                    #if conArea / recArea < 0.2 or conArea < 400 or highpArea / conArea < 0.2:
                    #if conArea / recArea < 0.3 or conArea < 200 or highpArea / conArea < 0.3:
                    if conArea / recArea < 0.1 or conArea < 200 or highpArea / conArea < 0.1:
                        keep.append(0)
    #                elif highpArea / conArea > 0.9:
    #                    keep.append(0)
    #                    boxs2, recs2, keep2 = obtain_box(p[y:y+h,x:x+w], 0.8, 0.9)
    #                    for idx2, b2 in enumerate(boxs2):
    #                        recs2[idx2][0] += w
    #                        recs2[idx2][1] += h
    #    
    #                        boxs.append(b2 + np.asarray([x,y]))
    #                        rects.append( recs2[idx2] )
    #                        keep.append( keep2[idx2] )
                    else:
                        keep.append(1)
                        if do_split:
                            split_top(curp, boxs, recs, keep, conArea)

        remove_dup(boxs, recs, keep)

    except (KeyboardInterrupt, SystemExit):
        raise
    except:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        for info in sys.exc_info():
          sys.stdout.write("[error]: Unexpected error: %s\n" % (info))

    return boxs, recs, keep


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('command', type=str, help='json | heat | rec | eval')
    parser.add_argument('--nj', type=int, default=1)
    parser.add_argument('--nthj', type=int, default=0)
    parser.add_argument('--proto', type=str, required=True)
    parser.add_argument('--model', type=str, required=True)
    parser.add_argument('--list_file', type=str, required=True)
    parser.add_argument('--gpu_id', type=int, required=False)
    parser.add_argument('--min_area', type=float, default=200)
    parser.add_argument('--mean0', type=float, required=True)
    parser.add_argument('--mean1', type=float, required=True)
    parser.add_argument('--mean2', type=float, required=True)
    parser.add_argument('--imscale', type=float, default=1.0)
    parser.add_argument('--thresh', type=float, default=0.5)
    parser.add_argument('--thresh2', type=float, default=0.7)
    parser.add_argument('--keep_filtered', type=distutils.util.strtobool, default=False)
    parser.add_argument('--is_cascaded', type=distutils.util.strtobool, default=False)
    parser.add_argument('--do_split', type=distutils.util.strtobool, default=False)
    parser.add_argument('--do_append', type=distutils.util.strtobool, default=False)
    parser.add_argument('--output_blob_name', nargs='+', type=str, default=['score'], required=True)
    parser.add_argument('--scale_fm', nargs='+', type=float, default=[1.], required=True)

    args = parser.parse_args()

    if len(args.output_blob_name) != len(args.scale_fm):
        print '#output_blob_name (%d) != #scale_fm (%d)' % (len(args.output_blob_name), len(args.scale_fm))
        sys.exit(1)

    resultarea = 0.
    trutharea = 0.
    correctarea = 0.

    resultproposal = 0.
    truthproposal = 0.
    correctproposal_precision = 0.
    correctproposal_recall = 0.

    correctproposal_precision2 = 0.
    correctproposal_recall2 = 0.

    norm_precision = 0.
    norm_recall = 0.


    net = caffe.Net(args.proto, args.model, caffe.TEST)
    if args.gpu_id != None:
        caffe.set_mode_gpu()
        caffe.set_device(args.gpu_id)
    else:
        caffe.set_mode_cpu()

    print "[info] <%s> : Start ..." % (str(datetime.now()))
    with open(args.list_file, 'r') as listf:
        for idx, line in enumerate(listf):
            if idx % args.nj != args.nthj:
                continue
            try:
                file_result = line.split()
                if len( file_result ) != 2 and not args.is_cascaded or len( file_result ) != 3 and args.is_cascaded:
                    sys.stdout.write("[error]: incorrect line: %s with %d fields\n" % (line, len(file_result)))
                    continue

                if file_result[0].startswith( 'http' ):
                    image_orig = url_to_image(file_result[0])
                else:
                    image_orig = cv2.imread(file_result[0])
                shape_orig = image_orig.shape
                #scale = min(600.0 / image_orig.shape[0], 600.0 / image_orig.shape[1])
                scale = args.imscale
                image_orig = cv2.resize(image_orig, (int(shape_orig[1] * scale), int(shape_orig[0] * scale)))

                image_orig = image_orig.astype(np.float32)
                image = np.copy(image_orig)

                ps = cal_prob(net, image, args.output_blob_name)

                prev_res = None
                if args.is_cascaded:
                    with open(file_result[2], 'r') as prev_resf:
                        prev_res = json.load(prev_resf)['Rects']

                #print file_result[0], p.shape, image_orig.shape
                for sidx, p in enumerate(ps):
                    p = cv2.resize(p, None, fx=args.scale_fm[sidx], fy=args.scale_fm[sidx])
                    p = p[0:image_orig.shape[0], 0:image_orig.shape[1]]
                    ps[sidx] = p

                if args.command != 'heat':
                    boxs, recs, keep = obtain_box(ps, args.thresh, args.thresh2, prev_res, args.do_split)

                    if args.do_append:
                        with open(file_result[1], 'r') as prevf:
                            jsons = json.load(prevf)
                    else:
                        jsons = {'ImagePath': file_result[0], 'Rects': [], 'Polys': [], 'Shape': [shape_orig[1], shape_orig[0]]}
                    rec_image = np.copy(image_orig)
                    for bidx, box in enumerate(boxs):
                        if args.command == 'rec':
                            color = [(0,0,255), (255,0,0)]
                            boxi = np.int0( np.int0(box) / scale )
                            cv2.drawContours(rec_image, [boxi], 0, color[ keep[bidx] ], 2)
                        elif args.command == 'json' or args.command == 'eval':
                            if keep[bidx] == 1 or args.keep_filtered:
                                skip_cur = False
                                x, y, w, h = np.asarray(recs[bidx]) / scale

                                for box0 in jsons['Polys']:
                                    if np.max( np.abs(np.float32(box / scale) - box0) ) < 1. or np.max(np.abs(box[0] - box[1])) < 1. or np.max(np.abs(box[1] - box[2])) < 1. or np.max(np.abs(box[2] - box[3])) < 1. or np.max(np.abs(box[3] - box[0])) < 1. or w * h < args.min_area:
                                        skip_cur = True
                                        break
                                if skip_cur:
                                    continue

                                jsons['Rects'].append({'x':x, 'y':y, 'height':h, 'width':w})
                                jsons['Polys'].append((np.float32(box) / scale).tolist())
                        else:
                            break

                if args.command == 'json':
                    makeparrentdir(file_result[1])
                    with open(file_result[1], 'w') as ofile:
                        json.dump(jsons, ofile)
                
                elif args.command == 'rec':
                    makeparrentdir(file_result[1])
                    cv2.imwrite(file_result[1], rec_image)
                    
                elif args.command == 'heat':
                    heat_img = np.zeros( p.shape )
                    R,G,B = rgb_heat(0, 1, p)
                    height = min(image_orig.shape[0], p.shape[0])
                    width = min(image_orig.shape[1], p.shape[1])
                    heat_img = image_orig[0:height,0:width,:] * 0.2 + cv2.resize(np.asarray([B,G,R]).transpose((1,2,0))[0:height,0:width,:], (shape_orig[1], shape_orig[0])) * 0.8
                    cv2.imwrite(file_result[1], heat_img)

                elif args.command == 'eval':
                    with open(file_result[1], 'r') as truthf:
                        truthdata = json.load(truthf)
                    resultdata = jsons

                    resultPolys = []
                    truthPolys  = []
                    for poly in resultdata['Polys']:
                        resultPolys.append( Polygon([(x,y) for x,y in poly]) )
                    for poly in truthdata['Polys']:
                        truthPolys.append( Polygon([(x,y) for x,y in poly]) )

                    resultproposal = resultproposal + len(resultPolys)
                    truthproposal = truthproposal + len(truthPolys)
                    corpr = np.zeros(len(resultPolys))
                    corrc = np.zeros(len(truthPolys))
                    corpr2 = np.zeros(len(resultPolys))
                    corrc2 = np.zeros(len(truthPolys))
                    overlapPolygon_res = [Polygon() for i in range(len(resultPolys))]
                    overlapIdx_res = [[] for i in range(len(resultPolys))]
                    overlapPolygon_tru = [Polygon() for i in range(len(truthPolys))]
                    overlapIdx_tru = [[] for i in range(len(truthPolys))]
                    for idx1,poly in enumerate(resultPolys):
                        if poly.area < 1:
                            continue
                        for idx2,poly2 in enumerate(truthPolys):
                            if poly2.area < 1:
                                continue
                            try:
                                t_interarea = poly.intersection(poly2).area
                                correctarea += t_interarea
                                if t_interarea / (poly2.area + poly.area - t_interarea) > 0.5:
                                    corpr[idx1] = 1
                                    corrc[idx2] = 1
                                    corpr2[idx1] = 1;
                                    corrc2[idx2] = 1
                                    #correctproposal_precision2 += 1
                                else:
                                    if t_interarea / poly2.area > 0.75:
                                        corrc2[idx2] = 1
                                        overlapPolygon_res[idx1].union(poly2)
                                        overlapIdx_res[idx1].append(idx2)
                                    if t_interarea / poly.area > 0.75:
                                        corpr2[idx1] = 1;
                                        #correctproposal_precision2 += 1
                                        overlapPolygon_tru[idx2].union(poly)
                                        overlapIdx_tru[idx2].append(idx1)

                            except TopologicalError:
                                print poly
                                print poly2
                        resultarea += poly.area

                    for poly2 in truthPolys:
                        trutharea = trutharea + poly2.area

                    correctproposal_precision += np.sum(corpr)
                    correctproposal_recall += np.sum(corrc)

                    for idx1 in range(0, len(overlapPolygon_res)):
                        t_interarea = resultPolys[idx1].intersection(overlapPolygon_res[idx1]).area
                        if overlapPolygon_res[idx1].area != 0 and t_interarea / (resultPolys[idx1].area + overlapPolygon_res[idx1].area - t_interarea) > 0.5:
                            corpr2[idx1] = 1
                    for idx2 in range(0, len(overlapPolygon_tru)):
                        t_interarea = truthPolys[idx2].intersection(overlapPolygon_tru[idx2]).area
                        if overlapPolygon_tru[idx2].area != 0 and t_interarea / (truthPolys[idx2].area + overlapPolygon_tru[idx2].area - t_interarea) > 0.5:
                            corrc2[idx1] = 1

                    correctproposal_precision2 += np.sum(corpr2)
                    correctproposal_recall2 += np.sum(corrc2)


                    sys.stdout.write('-')
                    sys.stdout.flush()
                    if (1+idx) % 10 == 0:
                        print "%5d images: %0.5f %0.5f | %0.5f %0.5f | %0.5f %0.5f" % (
                            idx , correctarea / resultarea, correctarea / trutharea ,
                            correctproposal_precision / resultproposal, correctproposal_recall / truthproposal,
                            correctproposal_precision2 / resultproposal, correctproposal_recall2 / truthproposal
                        )


            except (KeyboardInterrupt, SystemExit):
                raise
            except:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
                sys.stdout.write("[error]: while dealing with record: %s\n" % (line))
                for info in sys.exc_info():
                  sys.stdout.write("[error]: Unexpected error: %s\n" % (info))
 
    if args.command == 'eval':
        print "%0.5f %0.5f | %0.5f %0.5f | %0.5f %0.5f" % (
            correctarea / resultarea, correctarea / trutharea ,
            correctproposal_precision / resultproposal, correctproposal_recall / truthproposal,
            correctproposal_precision2 / resultproposal, correctproposal_recall2 / truthproposal
        )
