import sqlalchemy
from sqlalchemy import orm
from sqlalchemy import MetaData, Table, Column, Integer
from sqlalchemy.ext.declarative import declarative_base
from models.entry import Entry
import argparse
from datetime import datetime

DATABASE_USER = 'root'
DATABASE_HOST = '10.6.96.23'
DATABASE_PASS = '3rdTripod'
DATABASE_NAME = 'happy_tagger'

#FILTER = {'volume_id':203, 'value1':1085}    # domestic.vulgar
#FILTER = {'volume_id':203, 'value1':2}        # domestic.neg
#FILTER = {'volume_id':204, 'value1':0}       # international.unlabeled
#FILTER = {'volume_id':205, 'value1':1085}    # domestic.second.vulgar
#FILTER = {'volume_id':205, 'value1':2}    # domestic.second.neg
#FILTER = {'volume_id':206, 'value1':1085}    # domestic.third.vulgar
FILTER = {'volume_id':206, 'value1':2}    # domestic.third.neg

#FILTER = {'volume_id':192, 'value1':1075}    # mirror_selfie
#FILTER = {'volume_id':208, 'value1':1085}    # vulgar.checkprecition
#FILTER = {'volume_id':208, 'value1':2}    # vulgar.normal.checkprecition
FILTER = {'volume_id':207, 'value1':1}    # porn.second.normal

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--list_file', type=str, required=True)
    args = parser.parse_args()

    engine = sqlalchemy.create_engine(
        'mysql+pymysql://%s:%s@%s:3306/%s?charset=utf8' %
        (DATABASE_USER,
         DATABASE_PASS,
         DATABASE_HOST,
         DATABASE_NAME),
        pool_recycle = 286,
        echo = False)

    print '[info] %s: start' % (str(datetime.now()))
    with open(args.list_file, 'r') as f:
        values = []
        for idx, line in enumerate(f):
            line = line.strip()
            fields = line.split(' ')
            value1 = 0
            if len(fields) >= 4:
                value1 = int(fields[3])
            if len( fields ) != 0:
                values.append( (int(fields[0]), fields[1], fields[2], value1) )
                
            if idx % 100 == 99:
                with engine.connect() as conn:
                    conn.execute("INSERT INTO `entries` (volume_id, original_url, thumbnail_url, value1) values (%s, %s, %s, %s)", values)
                print "[info] %s: processed %d / %d items" % (str(datetime.now()), len(values), idx)
                values = []
        if len(values) != 0:
            with engine.connect() as conn:
                conn.execute("INSERT INTO `entries` (volume_id, original_url, thumbnail_url, value1) values (%s, %s, %s, %s)", values)

print '[info] %s: done' % (str(datetime.now()))



# whith engine.connect as conn:
# 
# 
# SessionStat = orm.sessionmaker(
#     bind = engine,
#     expire_on_commit=False
# )
# 
# session_stat = SessionStat()
# Base = declarative_base()
# 
# alldata = session_stat.query(Entry).filter_by(**FILTER).all()
# for d in alldata:
#     print(d.original_url)
