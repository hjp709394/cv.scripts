#remove fucking bad images
#images with single channel(gray scale image) will be removed as well.
import shutil, os
from PIL import Image
import numpy as np
import os
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--nj', type=int, default=1)
    parser.add_argument('--nthj', type=int, default=0)
    #parser.add_argument('--json', type=str, required=True)
    parser.add_argument('--flist', type=str, required=True)
    args = parser.parse_args()

    with open(args.flist) as flist:
        for idx, line in enumerate(flist):
            if idx % args.nj != args.nthj:
                continue
            line = line.strip().rstrip()
            try:
                im = Image.open(line)
            except IOError:
	        print '[error] could not read file: ' + line
		continue
	    
	    try:
	        im = np.array(im, dtype=np.float32)
            except (TypeError, SystemError):
	        print '[error] could not read file: ' + line
		continue
	    print line

