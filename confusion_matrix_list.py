import sys
import caffe
import numpy as np
import argparse
from collections import defaultdict
from PIL import Image
import math

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--nj', type=int, default=1)
    parser.add_argument('--nthj', type=int, default=0)
    parser.add_argument('--proto', type=str, required=True)
    parser.add_argument('--model', type=str, required=True)
    parser.add_argument('--list_file', type=str, required=True)
    parser.add_argument('--crop_size', type=int, required=True)
    parser.add_argument('--gpu_id', type=int, required=True)
    parser.add_argument('--mean0', type=int, required=True)
    parser.add_argument('--mean1', type=int, required=True)
    parser.add_argument('--mean2', type=int, required=True)
    args = parser.parse_args()

    mean0 = args.mean0
    mean1 = args.mean1
    mean2 = args.mean2

    count = 0
    correct = 0
    matrix = defaultdict(int) # (real,pred) -> int
    labels_set = set()

    net = caffe.Net(args.proto, args.model, caffe.TEST)
    #caffe.set_mode_cpu()
    caffe.set_mode_gpu()
    caffe.set_device(args.gpu_id)

    with open(args.list_file, 'r') as listf:
        for idx, line in enumerate(listf):
            if idx % args.nj != args.nthj:
                continue
            try:
                file_label = line.split()
                if (len( file_label ) != 2):
                    sys.stdout.write("[error]: incorrect line: %s with %d fields\n" % (line, len(file_label)))
                    continue

                label = int(file_label[1])

                image = Image.open(file_label[0])
                R, G, B = image.split()

                R = np.asarray(R).astype(int)
                G = np.asarray(G).astype(int)
                B = np.asarray(B).astype(int)
                image = np.asarray([B, G, R])
                
                start_idx_1 = (image.shape[1] - args.crop_size) / 2
                start_idx_2 = (image.shape[2] - args.crop_size) / 2
                image = image[:,start_idx_1:start_idx_1+args.crop_size, start_idx_2:start_idx_2 + args.crop_size]

                image[0,:,:] = image[0,:,:] - mean0
                image[1,:,:] = image[1,:,:] - mean1
                image[2,:,:] = image[2,:,:] - mean2

                out = net.forward_all(data=np.asarray([image]))
                plabel = int(out['prob'][0].argmax(axis=0))

                count = count + 1
                iscorrect = label == plabel
                correct = correct + (1 if iscorrect else 0)
                matrix[(label, plabel)] += 1
                labels_set.update([label, plabel])

                if count % 100 == 0:
                    print "[info]: processed %d files\tacc %.2f" % (count, 100.*correct/count)

                sys.stdout.write( "%s %d" % (file_label[0], label) )
                for idx in range(0, out['prob'].shape[1]):
                    sys.stdout.write( " %f" % (out['prob'][0][idx]) )
                sys.stdout.write( " %s\n" % file_label[0])

                sys.stdout.flush()
            except:
                sys.stdout.write("[error]: while dealing with record: %s\n" % (line))
                sys.stdout.write("[error]: Unexpected error: %s\n" % (sys.exc_info()[0]))
 
 
    print("[info]: " + str(correct) + " out of " + str(count) + " (" + str(100.*correct/count) + ") were classified correctly")

    print "[info]: "
    print "[info]: Confusion matrix:"
    print "[info]: (r , p) | count"
    for l in labels_set:
        for pl in labels_set:
            print "[info]: (%i , %i) | %i" % (l, pl, matrix[(l,pl)])

    sys.stdout.flush()
