import sys
import traceback
import caffe
import numpy as np
import argparse
from collections import defaultdict
from PIL import Image
import cv2
from matplotlib import pyplot as plt
import math

def show_img(img):
    plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB));
    plt.show();

def crop_image(img, size_crop, region):
    img_height, img_width = img.shape[0:2]
    if region[0] == 't':
        height_off = 0
    elif region[0] == 'c':
        height_off = (img_height - size_crop[0]) / 2
    elif region[0] == 'b':
        height_off = img_height - size_crop[0]
    else:
        print '[error]: incorrect region string: ' + region

    if region[1] == 'l':
        width_off = 0;
    elif region[1] == 'c':
        width_off = (img_width - size_crop[1]) / 2
    elif region[1] == 'r':
        width_off = img_width - size_crop[1]
    else:
        print '[error]: incorrect region string: ' + region
    return img[height_off:height_off+size_crop[0], width_off:width_off+size_crop[1], :]
    
    
def transform(img, min_dim=0, new_width=256, new_height=256, crop_size=224, rotate_degree=0, zoom=1, interpolation=cv2.INTER_LINEAR, region='cc', scale=1, brightness=0, smooth_size=0, sharpen_weight=0):
    """
    Parameters:
        view=cc|tl|tr|bl|br (center | top left | top right | bottom left | bottom right)
    """
    try:
        img = img.astype(np.float32)
        img_height, img_width = img.shape[0:2]
        if min_dim != 0:
            #print '[INFO] Using min_dim %d instead new_width, new_height' % ( min_dim )
            if img_height > img_width:
                new_width  = min_dim;
                new_height = new_width * img_height / img_width;
            else:
                new_height = min_dim;
                new_width  = new_height * img_width / img_height;
        img = cv2.resize(img, (int(new_width * zoom), int(new_height * zoom)), interpolation=interpolation)
        img_height, img_width = img.shape[0:2]

        if rotate_degree != 0:
            if img.shape[0] != img.shape[1]:
                img = crop_image(img, (min(img_height, img_width), min(img_height, img_width)), region)
                img_height, img_width = img.shape[0:2]
            max_angle = img_width / ( (crop_size + 2) / (math.sqrt(2.0) / 2) )
            if max_angle >= 1.0:
                max_angle = 45
            elif max_angle <= math.sqrt(2.0) / 2:
                max_angle = 0
            else:
                max_angle = 45 - math.acos(max_angle) * 180 / math.pi
            assert abs(rotate_degree) <= max_angle, ("[ERROR] rotate_degree must be less than %f for the current size of image." % (max_angle))
            rotmat = cv2.getRotationMatrix2D((img_width / 2, img_height / 2), rotate_degree, 1.0)
            img = cv2.warpAffine(img, rotmat, (img_width, img_height), flags=interpolation)

            valid_dim = img_width / math.cos( (45 - abs(rotate_degree)) * math.pi / 180 ) * math.sqrt(2.0) / 2
            img = img[(img_height-valid_dim)/2:(img_height+valid_dim)/2, (img_width-valid_dim)/2:(img_width+valid_dim)/2];
            img_height, img_width = img.shape[0:2]

        img = crop_image(img, (crop_size, crop_size), region)

        if smooth_size != 0:
            img_orig = img
            img = cv2.GaussianBlur(img_orig, (smooth_size, smooth_size), 0)
            if sharpen_weight != 0:
                img = cv2.addWeighted(img_orig, 1 + sharpen_weight, img, -sharpen_weight, 0)

        if scale != 1 or brightness != 0:
            img = img * scale + brightness

        img[img > 255] = 255
        img[img < 0] = 0
        img = img.astype(np.uint8)
    except:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        sys.stdout.write("[error]: Unexpected error: %s\n" % (sys.exc_info()[0]))
     
    return img

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--nj', type=int, default=1)
    parser.add_argument('--nthj', type=int, default=0)
    parser.add_argument('--proto', type=str, required=True)
    parser.add_argument('--model', type=str, required=True)
    parser.add_argument('--list_file', type=str, required=True)
    parser.add_argument('--crop_size', type=int, required=True)
    parser.add_argument('--gpu_id', type=int, required=True)
    parser.add_argument('--mean0', type=int, required=True)
    parser.add_argument('--mean1', type=int, required=True)
    parser.add_argument('--mean2', type=int, required=True)
    parser.add_argument('--smooth_size', type=int, default=0)
    parser.add_argument('--sharpen_weight', type=float, default=0)
    parser.add_argument('--rotate_degree', type=float, default=0)
    parser.add_argument('--zoom', type=float, default=1)
    parser.add_argument('--min_dim', type=int, default=0)
    parser.add_argument('--new_height', type=int, default=256)
    parser.add_argument('--new_width', type=int, default=256)
    parser.add_argument('--region', type=str, default='cc')
    parser.add_argument('--scale', type=float, default=1)
    parser.add_argument('--brightness', type=float, default=0)
    args = parser.parse_args()

#    with open(args.list_file, 'r') as listf:
#        for line in listf:
#            file_label = line.split()
#            file = file_label[0]
#            label = file_label[1]
#            img = cv2.imread(file)
#            img = transform(img, min_dim=224, zoom=1.2, rotate_degree=10, brightness=0, scale=1, smooth_size=5, sharpen_weight=1)
#            show_img(img)
#    sys.exit(0)

    mean0 = args.mean0
    mean1 = args.mean1
    mean2 = args.mean2

    count = 0
    correct = 0
    matrix = defaultdict(int) # (real,pred) -> int
    labels_set = set()

    net = caffe.Net(args.proto, args.model, caffe.TEST)
    #caffe.set_mode_cpu()
    caffe.set_mode_gpu()
    caffe.set_device(args.gpu_id)

    with open(args.list_file, 'r') as listf:
        for idx, line in enumerate(listf):
            if idx % args.nj != args.nthj:
                continue
            try:
                file_label = line.split()
                if (len( file_label ) != 2):
                    sys.stdout.write("[error]: incorrect line: %s with %d fields\n" % (line, len(file_label)))
                    continue

                label = int(file_label[1])

                image = cv2.imread(file_label[0])
                image = transform(image, new_height=args.new_height, new_width=args.new_width, crop_size=args.crop_size, rotate_degree=args.rotate_degree, zoom=args.zoom, interpolation=cv2.INTER_LINEAR, region=args.region, scale=args.scale, brightness=args.brightness, smooth_size=args.smooth_size, sharpen_weight=args.sharpen_weight)

                #if idx == 0:
                #    cv2.imwrite('debug/debug.jpg', image)

                # convert to [channel X width X height]
                image = np.asarray([image[:,:,0], image[:,:,1], image[:,:,2]], dtype=np.int32)

                image[0,:,:] = image[0,:,:] - mean0
                image[1,:,:] = image[1,:,:] - mean1
                image[2,:,:] = image[2,:,:] - mean2
                
                out = net.forward_all(data=np.asarray([image]))
                plabel = int(out['prob'][0].argmax(axis=0))

                count = count + 1
                iscorrect = label == plabel
                correct = correct + (1 if iscorrect else 0)
                matrix[(label, plabel)] += 1
                labels_set.update([label, plabel])

                if count % 100 == 0:
                    print "[info]: processed %d files\tacc %.2f" % (count, 100.*correct/count)

                sys.stdout.write( "%s %d" % (file_label[0], label) )
                for idx in range(0, out['prob'].shape[1]):
                    sys.stdout.write( " %f" % (out['prob'][0][idx]) )
                sys.stdout.write( "\n")

                sys.stdout.flush()
            except:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
                sys.stdout.write("[error]: while dealing with record: %s\n" % (line))
                sys.stdout.write("[error]: Unexpected error: %s\n" % (sys.exc_info()[0]))
 
 
    print("[info]: " + str(correct) + " out of " + str(count) + " (" + str(100.*correct/count) + ") were classified correctly")

    print "[info]: "
    print "[info]: Confusion matrix:"
    print "[info]: (r , p) | count"
    for l in labels_set:
        for pl in labels_set:
            print "[info]: (%i , %i) | %i" % (l, pl, matrix[(l,pl)])

    sys.stdout.flush()
