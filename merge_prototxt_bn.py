import sys
from pprint import pprint
import traceback
import argparse
from collections import defaultdict

def protobuf2dict(lines):
    cur_token = ''
    pre_token = ''
    for c in lines:
        if c == ' ' || c == "\t" || c == "\n" || c == "\r":
            continue
        elif c != ':' && c != '{' && c != '}':
            cur_token += c




if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--proto_in', type=str, required=True)
    parser.add_argument('--proto_out', type=str, required=True )
    args = parser.parse_args()

    with open(args.proto_in) as f:
        lines = f.read()
        for c in lines:
            print c
