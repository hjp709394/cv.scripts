import sqlalchemy
from sqlalchemy import orm
from sqlalchemy.ext.declarative import declarative_base
from models.entry import Entry
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--volume_id', type=int, required=True)
    parser.add_argument('--value1', type=int, required=True)
    args = parser.parse_args()

    DATABASE_USER = 'root'
    DATABASE_HOST = '10.6.96.23'
    DATABASE_PASS = '3rdTripod'
    DATABASE_NAME = 'happy_tagger'

    #FILTER = {'volume_id':203, 'value1':1085}    # domestic.vulgar
    #FILTER = {'volume_id':203, 'value1':2}        # domestic.neg
    #FILTER = {'volume_id':204, 'value1':0}       # international.unlabeled
    #FILTER = {'volume_id':205, 'value1':1085}    # domestic.second.vulgar
    #FILTER = {'volume_id':205, 'value1':2}    # domestic.second.neg
    #FILTER = {'volume_id':206, 'value1':1085}    # domestic.third.vulgar
    #FILTER = {'volume_id':206, 'value1':2}    # domestic.third.neg

    #FILTER = {'volume_id':192, 'value1':1075}    # mirror_selfie
    #FILTER = {'volume_id':208, 'value1':1085}    # vulgar.checkprecition
    #FILTER = {'volume_id':208, 'value1':2}    # vulgar.normal.checkprecition
    #FILTER = {'volume_id':207, 'value1':1}    # porn.second.normal

    FILTER = {'volume_id':args.volume_id, 'value1':args.value1} 

    engine = sqlalchemy.create_engine(
        'mysql+pymysql://%s:%s@%s:3306/%s?charset=utf8' %
        (DATABASE_USER,
         DATABASE_PASS,
         DATABASE_HOST,
         DATABASE_NAME),
        pool_recycle = 286,
        echo = False)

    SessionStat = orm.sessionmaker(
        bind = engine,
        expire_on_commit=False
    )

    session_stat = SessionStat()
    Base = declarative_base()

    alldata = session_stat.query(Entry).filter_by(**FILTER).all()
    for d in alldata:
        print(d.original_url)
