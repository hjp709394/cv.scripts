import cStringIO
import StringIO
import imghdr
import urllib2
import sys
import traceback
import numpy as np
import argparse
from collections import defaultdict
from PIL import Image
import cv2
from matplotlib import pyplot as plt
import math
from datetime import datetime
import urllib

def url_to_image(url):
    # download the image, convert it to a NumPy array, and then read
    # it into OpenCV format
    resp = urllib.urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    # return the image
    return image

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--img', type=str)
    parser.add_argument('--left', type=float)
    parser.add_argument('--right', type=float)
    parser.add_argument('--bottom', type=float)
    parser.add_argument('--top', type=float)
    parser.add_argument('--output', type=str)
    args = parser.parse_args()

    if args.img.startswith( 'http' ):
        image = url_to_image(args.img)
    else:
        image = cv2.imread(args.img)

    cv2.rectangle(image, (int(args.left * image.shape[1]), int(args.top * image.shape[0])), (int(args.right * image.shape[1]), int(args.bottom * image.shape[0])), (255, 0, 0), 3)

    print(image.shape)
    cv2.imwrite(args.output, image)



