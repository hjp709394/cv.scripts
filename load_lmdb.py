import sys
import caffe
import matplotlib
import numpy as np
import lmdb
import argparse
from collections import defaultdict

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
#    parser.add_argument('--proto', type=str, required=True)
#    parser.add_argument('--model', type=str, required=True)
    parser.add_argument('--lmdb', type=str, required=True)
#    parser.add_argument('--lmdb_label', type=str, required=True)
#    parser.add_argument('--crop_size', type=int, required=True)
    args = parser.parse_args()

    count = 0
    correct = 0
    matrix = defaultdict(int) # (real,pred) -> int
    labels_set = set()

#    print args.proto, args.model, args.lmdb
#    net = caffe.Net(args.proto, args.model, caffe.TEST)
    #caffe.set_mode_cpu()
#    caffe.set_mode_gpu()
#    caffe.set_device(3)
    lmdb_env = lmdb.open(args.lmdb)
    lmdb_txn = lmdb_env.begin()
    lmdb_cursor = lmdb_txn.cursor()

#    lmdb_label = lmdb.open(args.lmdb_label)
#    lmdb_txn_label = lmdb_label.begin()
#    lmdb_cursor_label = lmdb_label.cursor()

    for key, value in lmdb_cursor:
        datum = caffe.proto.caffe_pb2.Datum()
        datum.ParseFromString(value)
        label = int(datum.label)
        prob = caffe.io.datum_to_array(datum)
        prob = prob.flatten()
        print key, label, prob
        #image = image.astype(np.uint8)
        #start_idx_1 = (image.shape[1] - args.crop_size) / 2
        #start_idx_2 = (image.shape[2] - args.crop_size) / 2
        #image = image[:,start_idx_1:start_idx_1+args.crop_size, start_idx_2:start_idx_2 + args.crop_size]
        #image[0,:,:] = image[0,:,:] - 104
        #image[1,:,:] = image[1,:,:] - 117
        #image[2,:,:] = image[2,:,:] - 123

        #out = net.forward_all(data=np.asarray([image]))
        #plabel = int(prob[0].argmax(axis=0))
#        plabel = int(prob.argmax(axis=0))
#
#        count = count + 1
#        iscorrect = label == plabel
#        correct = correct + (1 if iscorrect else 0)
#        matrix[(label, plabel)] += 1
#        labels_set.update([label, plabel])

#        if count % 200 == 0:
#            print "processed %d files\tacc %.2f" % (count, 100.*correct/count)
        #if not iscorrect:
        #    print("\rError: key=%s, expected %i but predicted %i" \
        #        % (key, label, plabel))

        #sys.stdout.write("\rAccuracy: %.1f%% of %d files" % (100.*correct/count, count))
        #sys.stdout.flush()

#    print(str(correct) + " out of " + str(count) + " were classified correctly")
#
#    print ""
#    print "Confusion matrix:"
#    print "(r , p) | count"
#    for l in labels_set:
#        for pl in labels_set:
#            print "(%i , %i) | %i" % (l, pl, matrix[(l,pl)])
