 # -*- coding: UTF-8 -*-
import signal
import re
import os
import sys
import argparse
from multiprocessing import Process
import requests
import hashlib

MAX_TRY = 3
PROCESS_NUM = 10
processes = []

def signal_handler(signum, frame):
  print 'finish....'
  for p in processes:
    p.terminate()
  sys.exit(0)

def get_image_data(url):
    if not url:
        return None

    ntry = 0
    r = None
    while ntry < MAX_TRY:
        try:
            r = requests.get(url, timeout=2)
            if r and r.status_code == requests.codes.ok:
                break
            ntry += 1
        except Exception as e:
            print "url {} not found, attempt times: {}, err {}".format(
                url, ntry, e)
            ntry += 1

    if not r or r.status_code != 200:
        return None
    return r.content

def get_html(url_path):
  print 'Fetching html...'
  r = requests.get(url_path)
  if not r or r.status_code != requests.codes.ok:
    return None
  return r.content

def get_image_urls(html_content):
  print 'Parsing html...'
  exp = 'objURL":"([a-z.:/_A-Z0-9]*)"'
  image_urls = re.findall(exp, html_content)
  print '{} images found in this page'.format(len(image_urls))
  return image_urls

def download(pid, urls, output_dir):
  for url in urls:
    data = get_image_data(url)
    if not data:
      continue
    md5 = hashlib.md5(data).hexdigest()
    fn = os.sep.join([output_dir, md5 + '.jpg'])
    with open(fn, 'w') as f:
      f.write(data)

def worker_start(key_word, output_dir, image_num):
  image_urls = []
  idx = 0
  while len(image_urls) < image_num:
    url = r'http://images.baidu.com/search/flip?tn=baiduimage&ie=utf-8&word={}&pn={}&gsm=0'.format(key_word, idx * 30)
    idx += 1
    print url
    html_content = get_html(url)
    if not html_content:
      continue
    html_content = str(html_content)
    urls = get_image_urls(html_content)
    image_urls.extend(urls)

  image_urls = image_urls[:min(len(image_urls), image_num)]

  urls = [[] for _ in xrange(PROCESS_NUM)]
  for i, url in enumerate(image_urls):
    which = i % PROCESS_NUM
    urls[which].append(url)

  if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

  for i in xrange(PROCESS_NUM):
    p = Process(target=download, name="#P{}".format(i),
        args=(i, urls[i], output_dir))
    p.daemon = True
    p.start()
    processes.append(p)

  for p in processes:
    p.join()

if __name__ == '__main__':
  signal.signal(signal.SIGINT, signal_handler)
  signal.signal(signal.SIGTERM, signal_handler)

  parser = argparse.ArgumentParser()
  parser.add_argument(
    "--word", "-w",
    default="",
    help="Key word to search"
  )

  parser.add_argument(
    "--output_dir", "-out",
    help="Destination file to store downloaded images"
  )

  parser.add_argument(
    "--image_num", '-n',
    default=1000,
    type=int,
    help="Num of images to download"
  )

  args = parser.parse_args()
  if len(args.word) == 0:
    print 'key word can not be empty'
    sys.exit(0)

  worker_start(args.word, args.output_dir, args.image_num)
