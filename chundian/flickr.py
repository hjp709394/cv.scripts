import re
import os
import signal
import sys
import argparse
#import pycurl
from multiprocessing import Process
import requests
#import uuid
import hashlib

MAX_TRY = 3
PROCESS_NUM = 10
processes = []

def signal_handler(signum, frame):
  print 'finish....'
  for p in processes:
    p.terminate()
  sys.exit(0)

def get_html(url_path):
  print('Fetching html...')
  nretry = 0
  r = None
  while nretry < 3:
    try:
      r = requests.get(url_path)
      break
    except Exception, e:
      print('error: {}\n'.format(e))
      nretry += 1

  if not r or r.status_code != requests.codes.ok:
    return None
  return r.content

def get_image_urls(html_content):
  print('Parsing html...')

  #p = re.compile(r'"displayUrl":"([a-z.:\/_A-Z0-9]*)"')
  p = re.compile(r'"displayUrl":"([a-z.:\\//_A-Z0-9]*)"')
  image_urls = re.findall(p, html_content)

  print('%d images found in this page'%(len(image_urls)))
  for idx, item in enumerate(image_urls):
    image_urls[idx] = item[4:]
  return image_urls

header = {
      'Accept': ('text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'
        'image/webp,*/*;q=0.8'),
      'Accept-Encoding': 'gzip, deflate, sdch',
      'Accept-Language': 'zh-CN,zh;q=0.8,en;q=0.6,zh-TW;q=0.4',
      'cache-control': 'max-age=0',
      #'Connection': 'keep-alive',
      'upgrade-insecure-requests':1,
      'cookie': 'BX=fh719qdat5hhr&b=3&s=jp; xb=067194; localization=zh-hk%3Bxx%3Bjp; flrbp=1439876667-6c50dc284d34c75c56d1c0b3edc38000aea30d42; flrbrp=1439876667-aef11686b39a98ac6e41ae7374e695a3748918c2; flrbs=1439876667-a0ba655a86c6a3a23a6ad818f79baa007869c347; flrbra=1439876667-4920de214cf73d7128393d8fc25e2ed2d752740c; flrbrf=1439876667-1d7f7961540ab60d8e9f67efe6cd4f32af7bd464; flrbgrp=1439876667-e7b6fd3da010a9bf1d8e9ff5848586624acfbed6; flrbgdrp=1439876667-37abf6ef376cabacab1ed094e630898543c24e94; flrbgmrp=1439876667-a0d60ac4592d2c6a5ac8709dc08f630980689062; flrbre=1439876667-9e24c18a44a01bd01afac1191284eba0ccfea099; flrbras=1439876667-7ce59092a5c56876fdeb9ce86f1158aac324d55f; flrbrgs=1439876667-c17cf7f4bccd9ad79c40e127255d66a58437fb83; flrbrps=1439876667-84491b9426b97abc01a7dc5d7f27d9beed693740; flrbcr=1439876667-1e4a083e655d5e5cbddd4405cef100ac6bffd8e3; ywandp=10001109650879%3A556395467; fpc=10001109650879%3AZcWP312Q%7C%7C; adCounter=5; vp=1280%2C218%2C2%2C0%2Csearch-photos-everyone-view%3A1020',
      'user-agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36'
      }

def download(pid, urls, output_dir):
  for url in urls:
    url_ = url
    if not url_.endswith('jpg') or url_.endswith('jpeg'):
      continue

    r = None
    retries = 3
    nretry = 0
    while nretry < retries:
      try:
        r = requests.get(url, headers=header)
        break
      except Exception, e:
        print('error: {}'.format(e))
        nretry += 1

    if r is None or r.status_code != requests.codes.ok:
      return

    data = r.content
    md5 = hashlib.md5(data).hexdigest()
    fn = os.sep.join([output_dir, md5 + '.jpg'])
    if os.path.isfile(fn):
      continue
    print fn
    with open(fn, 'w') as f:
      f.write(data)

def worker_start(key_word, output_dir, image_num):
  image_urls = []
  idx = 0
  while True:
    url = 'https://www.flickr.com/search/?text={}&page={}'.format(key_word, idx)
    idx += 1
    html_content = get_html(url)
    if not html_content:
      continue
    html_content = str(html_content)
    urls = get_image_urls(html_content)
    image_urls.extend(urls)

    if len(image_urls) >= image_num:
      break

  mp ={}
  for url in image_urls:
    p1 = url.find('com')
    p2 = url.rfind('/')
    img = url[p1+5:p2-1]
    if img not in mp:
      mp[img] = set()
    url = [c for c in url if c != '\\']
    url = 'http://' + ''.join(url)
    mp[img].add(url)

  image_urls = []
  for key, val in mp.iteritems():
    l = 0
    which = None
    for img in val:
      if l == 0 or len(img) < l:
        l, which = len(img), img
    which2 = which[:-4] + '_z' + '.jpg'
    if which2 in val:
      image_urls.append(which2)
    else:
      image_urls.append(which)

  print len(image_urls)
  urls = [[] for _ in xrange(PROCESS_NUM)]
  for i, url in enumerate(image_urls):
    which = i % PROCESS_NUM
    urls[which].append(url)

  if not os.path.isdir(output_dir):
    os.mkdir(output_dir)

  for i in xrange(PROCESS_NUM):
    p = Process(target=download, name="#P{}".format(i),
        args=(i, urls[i], output_dir))
    p.daemon = True
    p.start()
    processes.append(p)

  for p in processes:
    p.join()

if __name__ == '__main__':
  signal.signal(signal.SIGINT, signal_handler)
  signal.signal(signal.SIGTERM, signal_handler)

  parser = argparse.ArgumentParser()
  parser.add_argument(
    "--word", "-w",
    default="",
    help="Key word to search"
  )

  parser.add_argument(
    "--output_dir", "-out",
    help="Destination file to store downloaded images"
  )

  parser.add_argument(
    "--image_num", '-n',
    default=1000,
    type=int,
    help="Num of images to download"
  )

  args = parser.parse_args()
  if len(args.word) == 0:
    print 'key word can not be empty'
    sys.exit(0)

  worker_start(args.word, args.output_dir, args.image_num)
