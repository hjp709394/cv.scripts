import cv2
import sys
import traceback
import caffe
import numpy as np
import argparse
from collections import defaultdict
from PIL import Image
from matplotlib import pyplot as plt
import math
from datetime import datetime
import urllib
from pprint import pprint
from shapely.geometry import Polygon
from shapely.geos import TopologicalError
import os.path

import json

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--nj', type=int, default=1)
    parser.add_argument('--nthj', type=int, default=0)
    parser.add_argument('--list_file', type=str, required=True)
    args = parser.parse_args()

    resultarea = 0.
    trutharea = 0.
    correctarea = 0.

    resultproposal = 0.
    truthproposal = 0.
    correctproposal_precision = 0.
    correctproposal_recall = 0.

    correctproposal_precision2 = 0.
    correctproposal_recall2 = 0.

    norm_precision = 0.
    norm_recall = 0.

    print "[info] <%s> : Start ..." % (str(datetime.now()))
    with open(args.list_file, 'r') as listf:
        for idx, line in enumerate(listf):
            if idx % args.nj != args.nthj:
                continue
            try:
                truth_result = line.split()
                if (len( truth_result ) != 2):
                    sys.stdout.write("[error]: incorrect line: %s with %d fields\n" % (line, len(truth_result)))
                    continue
                # if not os.path.isfile(truth_result[0]):
                #     print 'file not found:', truth_result[0]
                #     continue 
                # if not os.path.isfile(truth_result[1]):
                #     print 'file not found:', truth_result[1]
                #     continue
                with open(truth_result[0], 'r') as truthf:
                    truthdata = json.load(truthf)
                with open(truth_result[1], 'r') as resultf:
                    resultdata = json.load(resultf)

                resultPolys = []
                truthPolys  = []
                for poly in resultdata['Polys']:
                    resultPolys.append( Polygon([(x,y) for x,y in poly]) )
                for poly in truthdata['Polys']:
                    truthPolys.append( Polygon([(x,y) for x,y in poly]) )

                resultproposal = resultproposal + len(resultPolys)
                truthproposal = truthproposal + len(truthPolys)
                corpr = np.zeros(len(resultPolys))
                corrc = np.zeros(len(truthPolys))
                corrc2 = np.zeros(len(truthPolys))
                overlapPolygon = [Polygon() for i in range(len(resultPolys))]
                overlapIdx = [[] for i in range(len(resultPolys))]
                for idx1,poly in enumerate(resultPolys):
                    if poly.area < 1:
                        continue
                    for idx2,poly2 in enumerate(truthPolys):
                        if poly2.area < 1:
                            continue
                        try:
                            t_interarea = poly.intersection(poly2).area
                            correctarea += t_interarea
                            if t_interarea / (poly2.area + poly.area - t_interarea) > 0.5:
                                corpr[idx1] = 1
                                corrc[idx2] = 1
                            if t_interarea / poly2.area > 0.75:
                                overlapPolygon[idx1] = overlapPolygon[idx1].union(poly2)
                                overlapIdx[idx1].append(idx2)
                        except TopologicalError:
                            print poly
                            print poly2
                    resultarea += poly.area

                    t_interarea = poly.intersection(overlapPolygon[idx1]).area
                    if overlapPolygon[idx1].area >= 1 and t_interarea / (overlapPolygon[idx1].area + poly.area - t_interarea) > 0.5:
                        correctproposal_precision2 += 1
                        for idx2 in overlapIdx[idx1]:
                            corrc2[idx2] = 1
                        
                for poly2 in truthPolys:
                    trutharea = trutharea + poly2.area

                correctproposal_precision += np.sum(corpr)
                correctproposal_recall += np.sum(corrc)
                correctproposal_recall2 += np.sum(corrc2)

                if idx % 100 == 0 and idx != 0:
                    print "%d images: %0.5f %0.5f | %0.5f %0.5f | %0.5f %0.5f" % (
                        idx , correctarea / resultarea, correctarea / trutharea ,
                        correctproposal_precision / resultproposal, correctproposal_recall / truthproposal,
                        correctproposal_precision2 / resultproposal, correctproposal_recall2 / truthproposal
                    )
                        
            except (KeyboardInterrupt, SystemExit):
                raise
            except:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
                sys.stdout.write("[error]: while dealing with record: %s\n" % (line))
                for info in sys.exc_info():
                    sys.stdout.write("[error]: Unexpected error: %s\n" % (info))

    print "%0.5f %0.5f | %0.5f %0.5f | %0.5f %0.5f" % (
        correctarea / resultarea, correctarea / trutharea ,
        correctproposal_precision / resultproposal, correctproposal_recall / truthproposal,
        correctproposal_precision2 / resultproposal, correctproposal_recall2 / truthproposal
    )
 
