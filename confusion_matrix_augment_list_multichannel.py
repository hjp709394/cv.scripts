import sys
import traceback
import caffe
import numpy as np
import argparse
from collections import defaultdict
from PIL import Image
import cv2
from matplotlib import pyplot as plt
import math
from datetime import datetime
import urllib

def url_to_image(url):
    # download the image, convert it to a NumPy array, and then read
    # it into OpenCV format
    resp = urllib.urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    # return the image
    return image

def show_img(img):
    plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB));
    plt.show();

def crop_image(img, size_crop, region):
    img_height, img_width = img.shape[0:2]
    if region[0] == 't':
        height_off = 0
    elif region[0] == 'c':
        height_off = (img_height - size_crop[0]) / 2
    elif region[0] == 'b':
        height_off = img_height - size_crop[0]
    else:
        print '[error]: incorrect region string: ' + region

    if region[1] == 'l':
        width_off = 0;
    elif region[1] == 'c':
        width_off = (img_width - size_crop[1]) / 2
    elif region[1] == 'r':
        width_off = img_width - size_crop[1]
    else:
        print '[error]: incorrect region string: ' + region
    return img[height_off:height_off+size_crop[0], width_off:width_off+size_crop[1], :]
    
    
def transform(img, resize_area=0, min_dim=0, new_width=0, new_height=0, crop_size=0, rotate_degree=0, zoom=1, interpolation=cv2.INTER_LINEAR, region='cc', scale=1, brightness=0, smooth_size=0, sharpen_weight=0):
    """
    Parameters:
        view=cc|tl|tr|bl|br (center | top left | top right | bottom left | bottom right)
    """
    try:
        img = img.astype(np.float32)
        img_height, img_width = img.shape[0:2]
        if min_dim != 0:
            #print '[INFO] Using min_dim %d instead new_width, new_height' % ( min_dim )
            if img_height > img_width:
                new_width  = min_dim;
                new_height = min_dim * img_height / img_width;
            else:
                new_height = min_dim;
                new_width  = min_dim * img_width / img_height;
        if new_height != 0 and new_width != 0:
            img = cv2.resize(img, (int(new_width * zoom), int(new_height * zoom)), interpolation=interpolation)

        img_height, img_width = img.shape[0:2]

        if rotate_degree != 0:
            if img.shape[0] != img.shape[1]:
                img = crop_image(img, (min(img_height, img_width), min(img_height, img_width)), region)
                img_height, img_width = img.shape[0:2]
            max_angle = img_width / ( (crop_size + 2) / (math.sqrt(2.0) / 2) )
            if max_angle >= 1.0 or crop_size == 0:
                max_angle = 45
            elif max_angle <= math.sqrt(2.0) / 2:
                max_angle = 0
            else:
                max_angle = 45 - math.acos(max_angle) * 180 / math.pi
            assert abs(rotate_degree) <= max_angle, ("[ERROR] rotate_degree must be less than %f for the current size of image." % (max_angle))
            rotmat = cv2.getRotationMatrix2D((img_width / 2, img_height / 2), rotate_degree, 1.0)
            img = cv2.warpAffine(img, rotmat, (img_width, img_height), flags=interpolation)

            valid_dim = img_width / math.cos( (45 - abs(rotate_degree)) * math.pi / 180 ) * math.sqrt(2.0) / 2
            img = img[(img_height-valid_dim)/2:(img_height+valid_dim)/2, (img_width-valid_dim)/2:(img_width+valid_dim)/2];
            img_height, img_width = img.shape[0:2]

        if crop_size != 0:
            img = crop_image(img, (crop_size, crop_size), region)

        if smooth_size != 0:
            img_orig = img
            img = cv2.GaussianBlur(img_orig, (smooth_size, smooth_size), 0)
            if sharpen_weight != 0:
                img = cv2.addWeighted(img_orig, 1 + sharpen_weight, img, -sharpen_weight, 0)

        if scale != 1 or brightness != 0:
            img = img * scale + brightness

#         img[img > 255] = 255
#         img[img < 0] = 0

    except:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        sys.stdout.write("[error]: Unexpected error: %s\n" % (sys.exc_info()[0]))
     
    return img

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--nj', type=int, default=1)
    parser.add_argument('--nthj', type=int, default=0)
    parser.add_argument('--proto', type=str, required=True)
    parser.add_argument('--model', type=str, required=True)
    parser.add_argument('--list_file', type=str, required=True)
    parser.add_argument('--crop_size', type=int, default=0, required=False)
    parser.add_argument('--gpu_id', type=int, required=True)
    parser.add_argument('--mean0', type=int, required=True)
    parser.add_argument('--mean1', type=int, required=True)
    parser.add_argument('--mean2', type=int, required=True)
    parser.add_argument('--mean_file', type=str, default='', required=False)
    parser.add_argument('--mean_height', type=int, default=0)
    parser.add_argument('--mean_width', type=int, default=0)
    parser.add_argument('--smooth_size', type=int, default=0)
    parser.add_argument('--sharpen_weight', type=float, default=0)
    parser.add_argument('--rotate_degree', type=float, default=0)
    parser.add_argument('--zoom', type=float, default=1)
    parser.add_argument('--min_dim', type=int, default=0)
    parser.add_argument('--resize_area', type=int, default=0)
    parser.add_argument('--new_height', type=int, default=0)
    parser.add_argument('--new_width', type=int, default=0)
    parser.add_argument('--region', type=str, default='cc')
    parser.add_argument('--scale', type=float, default=1)
    parser.add_argument('--brightness', type=float, default=0)
    parser.add_argument('--output_blob_name', type=str, default='prob')
    parser.add_argument('--do_average', default=False, action='store_true')
    args = parser.parse_args()

#    with open(args.list_file, 'r') as listf:
#        for line in listf:
#            file_label = line.split()
#            file = file_label[0]
#            label = file_label[1]
#            img = cv2.imread(file)
#            img = transform(img, min_dim=224, zoom=1.2, rotate_degree=10, brightness=0, scale=1, smooth_size=5, sharpen_weight=1)
#            show_img(img)
#    sys.exit(0)

    count = 0
    correct = 0
    correct0 = 0
    matrix = defaultdict(int) # (real,pred) -> int
    labels_set = set()

    net = caffe.Net(args.proto, args.model, caffe.TEST)
    #caffe.set_mode_cpu()
    caffe.set_mode_gpu()
    caffe.set_device(args.gpu_id)

    mean_np = np.asarray([])
    if args.mean_file != '':
        mean_blob = caffe.io.caffe_pb2.BlobProto()
        mean_data = open(args.mean_file,'rb').read()
        mean_blob.ParseFromString(mean_data)
        mean_np = np.asarray(mean_blob.data).reshape( 3, args.mean_height, args.new_width )
        ### caffe: c X h X w -> opencv: h X w X c
        mean_np = np.rollaxis( np.rollaxis(mean_np, 2), 2 )
    
    print "[info] <%s> : Start ..." % (str(datetime.now()))
    with open(args.list_file, 'r') as listf:
        for idx, line in enumerate(listf):
            if idx % args.nj != args.nthj:
                continue
            try:
                file_label = line.split()
                if (len( file_label ) != 2):
                    sys.stdout.write("[error]: incorrect line: %s with %d fields\n" % (line, len(file_label)))
                    continue

                label = int(file_label[1])

                if file_label[0].startswith( 'http' ):
                    image = url_to_image(file_label[0])
                else:
                    image = cv2.imread(file_label[0])

                image = transform(image, min_dim=args.min_dim, new_height=args.new_height, new_width=args.new_width, crop_size=args.crop_size, rotate_degree=args.rotate_degree, zoom=args.zoom, interpolation=cv2.INTER_LINEAR, region=args.region, scale=args.scale, brightness=args.brightness, smooth_size=args.smooth_size, sharpen_weight=args.sharpen_weight)

                ### minus mean
                if mean_np.shape[0] != 0:
                    mean_np = crop_image(mean_np, (args.crop_size, args.crop_size), args.region)
                    image = image - mean_np
                else:
                    image[:,:,0] = image[:,:,0] - args.mean0
                    image[:,:,1] = image[:,:,1] - args.mean1
                    image[:,:,2] = image[:,:,2] - args.mean2

                ### in opencv: height X width X channel -> in caffe: channel X height X width
                image = np.rollaxis(image, 2)

                input_shape = [ net.blobs[net.inputs[0]].shape[i] for i in range(0, len(net.blobs[ net.inputs[0] ].shape)) ]
                if image.shape[1] != input_shape[2] or image.shape[2] != input_shape[3]:
                    net.blobs[net.inputs[0]].reshape( input_shape[0], image.shape[0], image.shape[1], image.shape[2] )
                    
                out = net.forward_all(data=np.asarray([image]))

                ov = out[args.output_blob_name][0].astype(np.float32)

                if args.do_average:
                    avg_softmax = ov.sum(axis=2).sum(axis=1) / ov.shape[1] / ov.shape[2]
                    avg_softmax = np.exp(avg_softmax) / np.exp(avg_softmax).sum()

                    softmax_avg = np.exp(ov)
                    softmax_avg = softmax_avg / softmax_avg.sum(axis=0)
                    softmax_avg = softmax_avg.sum(axis=2).sum(axis=1) / ov.shape[1] / ov.shape[2]

                    plabel = int(softmax_avg.argmax(axis=0))
                    plabel0 = int(avg_softmax.argmax(axis=0))
                    count = count + 1
                    iscorrect = label == plabel
                    iscorrect0 = label == plabel0
                    correct = correct + (1 if iscorrect else 0)
                    correct0 = correct0 + (1 if iscorrect0 else 0)

                    sys.stdout.write( "%s %d" % (file_label[0], label) )
                    for idx in range(0, softmax_avg.shape[0]):
                        sys.stdout.write( " %f" % (softmax_avg[idx]) )
                    sys.stdout.write(" ||")
                    for idx in range(0, avg_softmax.shape[0]):
                        sys.stdout.write( " %f" % (avg_softmax[idx]) )
                    sys.stdout.write( "\n")
                else:
                    plabel = int(out[args.output_blob_name][0].argmax(axis=0))
                    count = count + 1
                    iscorrect = label == plabel
                    correct = correct + (1 if iscorrect else 0)

                    sys.stdout.write( "[prob]: %s %d" % (file_label[0], label) )
                    for idx in range(0, out[args.output_blob_name].shape[1]):
                        sys.stdout.write( " %f" % (out[args.output_blob_name][0][idx]) )
                    sys.stdout.write( "\n")

                sys.stdout.flush()

                matrix[(label, plabel)] += 1
                labels_set.update([label, plabel])

                if count % 100 == 0:
                    if args.do_average:
                        print "[info] <%s> : processed %d files\tacc %.2f, acc0 %.2f" % (str(datetime.now()), count, 100.*correct/count, 100.*correct0/count) 
                    else:
                        print "[info] <%s> : processed %d files\tacc %.2f" % (str(datetime.now()), count, 100.*correct/count) 


                sys.stdout.flush()
            except:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
                sys.stdout.write("[error]: while dealing with record: %s\n" % (line))
                sys.stdout.write("[error]: Unexpected error: %s\n" % (sys.exc_info()[0]))
 
 
    print("[info] <" + str(datetime.now()) + "> : " + str(correct) + " out of " + str(count) + " (" + str(100.*correct/count) + ") were classified correctly. (softmax before average if do_average)")
    if args.do_average:
        print("[info] <" + str(datetime.now()) + "> : " + str(correct0) + " out of " + str(count) + " (" + str(100.*correct0/count) + ") were classified correctly. (softmax after average)")

    print "[info]: "
    print "[info]: Confusion matrix:"
    print "[info]: (r , p) | count"
    for l in labels_set:
        for pl in labels_set:
            print "[info]: (%i , %i) | %i" % (l, pl, matrix[(l,pl)])

    sys.stdout.flush()
