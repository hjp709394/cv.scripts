import cStringIO
import StringIO
import imghdr
import urllib2
import sys
import traceback
import numpy as np
import argparse
from collections import defaultdict
from PIL import Image
import cv2
from matplotlib import pyplot as plt
import math
from datetime import datetime
import urllib

def url_to_image(url):
    # download the image, convert it to a NumPy array, and then read
    # it into OpenCV format
    resp = urllib.urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    # return the image
    return image

def show_img(img):
    plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB));
    plt.show();

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--nj', type=int, default=1)
    parser.add_argument('--nthj', type=int, default=0)
    parser.add_argument('--list_file', type=str, required=True)
    args = parser.parse_args()

    with open(args.list_file, 'r') as listf:
        for idx, line in enumerate(listf):
            line = line.strip().rstrip()
            if idx % args.nj != args.nthj:
                continue

            fields = line.split()
            if len(fields) != 2:
                print '[error] incorrect line with : ', line
                continue
            file = fields[0]
            file_out = fields[1]

            try:
                if file.startswith( 'http' ):
                    response = urllib2.urlopen(file)
                    data = cStringIO.StringIO(response.read())
                else:
                    with open(file, 'r') as f:
                        data = cStringIO.StringIO(f.read())
                image_frames = Image.open(data)

                ith = 0
                palette = image_frames.getpalette()
                try:
                    while True:
                        image_frames.putpalette(palette)
                        new_image = Image.new('RGBA', image_frames.size)
                        new_image.paste(image_frames)
                        new_image.save(file_out + '_' + str(ith) + '.png')
                        ith += 1
                        image_frames.seek(image_frames.tell() + 1)
                except EOFError:
                    pass
                    print '[info] %s processed done' % (file)
            except:
                print '[error] ', file
                exc_type, exc_value, exc_traceback = sys.exc_info()
                traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
                sys.stdout.write("[error]: while dealing with record: %s\n" % (line))
                sys.stdout.write("[error]: Unexpected error: %s\n" % (sys.exc_info()[0]))


