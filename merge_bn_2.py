import sys
from pprint import pprint
import traceback
import caffe
import numpy as np
import argparse
from collections import defaultdict
from PIL import Image
import cv2
from matplotlib import pyplot as plt
import math

caffe.set_mode_cpu()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--proto_in', type=str, required=True)
    parser.add_argument('--proto_out', type=str, required=True )
    parser.add_argument('--model_in', type=str, required=True)
    parser.add_argument('--model_out', type=str, required=True)
    parser.add_argument('--eps', type=float, default=0.00001, required=False, help="The conv layers' names before bn")
    parser.add_argument('--conv_name_in', nargs='*', type=str, required=False, help="The conv layers' names before bn")
    parser.add_argument('--bn_name_in', nargs='*', type=str, required=False, help="The bn layers' names")
    parser.add_argument('--scale_name_in', nargs='*', type=str, required=False, help="The scale layers' names")
    parser.add_argument('--conv_name_out', nargs='*', type=str, required=False, help="The merged conv layers' names")
    args = parser.parse_args()

#    if args.layer_name_in is not None and args.layer_name_out is None or args.layer_name_in is None and args.layer_name_out is not None or args.layer_name_in is not None and args.layer_name_out is not None and len( args.layer_name_in ) != len( args.layer_name_out ):
#        print "[error] number of layer_name_in does not match with that of layer_name_out ( %d vs %d )" % ( len(args.layer_name_in), len(args.layer_name_out) )
#        sys.exit(1)

    params_in = {}
    print '[info] loading net_in ' + args.proto_in
    net_in  = caffe.Net(args.proto_in , args.model_in , caffe.TEST)
    for key in net_in.params:
        params_in[key] = []
        for pa in net_in.params[key]:
            params_in[key].append( np.asarray( pa.data[...]) )
    del net_in
    print '[info] loading net_out ' + args.proto_out
    net_out = caffe.Net(args.proto_out, caffe.TEST)
##    #net_out = caffe.Net(args.proto_out, args.model_in, caffe.TEST)
##    net_out = caffe.Net(args.proto_in, args.model_in, caffe.TEST)
##
##    for key in net_in.params:
##      print key
##      if key[0:4] == 'conv':
##        print np.asarray(net_in.params[key][0].data[...]).shape
##
#    lni = 'conv2/3x3/bn'
#    for param in params_in[lni]:
#        print np.asarray(param).shape
#
#    lni = 'conv2/3x3/bn/sc'
#    for param in params_in[lni]:
#        print np.asarray(param).shape
#
#    lni = 'conv2/3x3'
#    for param in params_in[lni]:
#        print np.asarray(param).shape

#    weight = np.asarray( params_in[lni][0] )
#    weight = np.transpose( weight.reshape( (weight.shape[0], -1) ) )
#    print weight.shape, np.prod(weight.shape)
#    U,s,V = np.linalg.svd(weight, full_matrices=True)
#    print U.shape, s.shape, V.shape
##
###    for key in net_out.params:
###        if key in net_in.params:
###            print '[info] model_in param          exist for layer: ' + key
###        else:
###            print '[info] model_in param does not exist for layer: ' + key
###
    print '[info] copying layers parameters'
    for key in net_out.params:
        lno = lni = key
        if key not in params_in:
            print '[warn] missing layer parameters for ' + lni
            continue

        is_dim_matched = True
        for idx in range( len( net_out.params[lno] ) ):
            if len(params_in[lni]) > idx and np.prod( params_in[lni][idx].shape ) != np.prod( net_out.params[lno][idx].data.shape ) :
                is_dim_matched = False
                break

        if not is_dim_matched:
            print '[warn] skip layer ' + lni + ' because of shape mismatching ' \
                    + str(params_in[lni][idx].shape) + ' -> ' \
                    + str(net_out.params[lno][idx].data.shape)
            continue

        print '[info] %s -> %s' % (lni, lno)
        for idx in range( len( net_out.params[lno] ) ):
            if len(params_in[lni]) > idx:
                print '[info] \t\t' + str(params_in[lni][idx].shape) + ' -> ' + str(net_out.params[lno][idx].data.shape)
                net_out.params[lno][idx].data[...] = params_in[lni][idx].reshape( net_out.params[lno][idx].data.shape )
            else:
                net_out.params[lno][idx].data[...] = 0


#    for idx, lni in enumerate( sorted(params_in.keys()) ):
#        if lni[0:4] != 'conv' and lni[0:2] != 'fc':
#            continue
#        print '[info] \t\t' + str(params_in[lni][0].shape)
#        reduced_dim = params_in[lni][0].shape[0] / 2
#        print '[info] \t\t If SVD reduces dim from ' + str(params_in[lni][0].shape[0]) + ' to ' + str( reduced_dim )
#
#        weight = np.asarray( params_in[lni][0] )
#        weight_reshaped = np.transpose( weight.reshape( (weight.shape[0], -1) ) )
#        U,s,V = np.linalg.svd(weight_reshaped, full_matrices=True)
#        print '[info] ' + lni + ' ratio:', np.sum(s[0:reduced_dim]) / np.sum(s)
#
    print '--------------------'
    if args.bn_name_in is not None:
        print '[info] converting layers parameters : ' 
        print '[info] #layers to be converted', len(args.bn_name_in)
        for idx in range( len(args.bn_name_in) ):
            nconvin = args.conv_name_in[idx]
            nbnin = args.bn_name_in[idx]
            if args.scale_name_in is not None:
                nscalein = args.scale_name_in[idx]
            else:
                nscalein = None
            
            nconvout = args.conv_name_out[idx]
            print '[info] %s %s %s -> %s' % (nconvin, nbnin, nscalein, nconvout)

            print '[info] conv_in %s has %d params' % (nconvin, len(params_in[nconvin]))
            convweight = np.asarray( params_in[nconvin][0] )
            convbias   = np.zeros( convweight.shape[0] )
            if len(params_in[nconvin]) > 1:
                convbias = params_in[nconvin][1]

            bnmeanp = np.asarray( params_in[nbnin][0] )
            bnvariancep = np.asarray( params_in[nbnin][1] )
            bnscale = 0
            if np.asarray(params_in[nbnin][2])[0] != 0:
                bnscale = 1. / np.asarray(params_in[nbnin][2])[0] 
            bnmeanp = bnmeanp * bnscale
            bnvariancep = bnvariancep * bnscale

            if args.scale_name_in is not None:
                scaleweight = np.asarray ( params_in[nscalein][0] )
                scalebias = np.asarray ( params_in[nscalein][1] )
                print '[info]', convweight.shape, convbias.shape, '|', bnmeanp.shape, bnvariancep.shape, '|', scaleweight.shape, scalebias.shape, '-->'

            ### merge BN layer
            # conv
            print len(convweight.shape)
            if len(convweight.shape) == 4:
                print '[info] conv'
                for channelOutIndex in range( convweight.shape[0] ):
                    convweight[channelOutIndex,:,:,:] = convweight[channelOutIndex,:,:,:] * (1 / np.sqrt(bnvariancep[channelOutIndex] + args.eps))
                    convbias[channelOutIndex] = convbias[channelOutIndex] * (1 / np.sqrt(bnvariancep[channelOutIndex] + args.eps)) + -bnmeanp[channelOutIndex] * (1 / np.sqrt(bnvariancep[channelOutIndex] + args.eps))
    
                ### merge scale layer
                if args.scale_name_in is not None:
                    for channelOutIndex in range( convweight.shape[0] ):
                        convweight[channelOutIndex,:,:,:] = convweight[channelOutIndex,:,:,:] * scaleweight[channelOutIndex]
                        convbias[channelOutIndex] = convbias[channelOutIndex] * scaleweight[channelOutIndex] + scalebias[channelOutIndex]
            # fc
            else:
                for channelOutIndex in range( convweight.shape[0] ):
                    convweight[channelOutIndex,:] = convweight[channelOutIndex,:] * (1 / np.sqrt(bnvariancep[channelOutIndex] + args.eps))
                    convbias[channelOutIndex] = convbias[channelOutIndex] * (1 / np.sqrt(bnvariancep[channelOutIndex] + args.eps)) + -bnmeanp[channelOutIndex] * (1 / np.sqrt(bnvariancep[channelOutIndex] + args.eps))
    
                ### merge scale layer
                if args.scale_name_in is not None:
                    for channelOutIndex in range( convweight.shape[0] ):
                        convweight[channelOutIndex,:] = convweight[channelOutIndex,:] * scaleweight[channelOutIndex]
                        convbias[channelOutIndex] = convbias[channelOutIndex] * scaleweight[channelOutIndex] + scalebias[channelOutIndex]

            net_out.params[nconvout][0].data[...] = convweight
            net_out.params[nconvout][1].data[...] = convbias

#            lno0, lno1 = args.layer_name_out[idx*2:idx*2+2]
#            print '[info] %s -> %s + %s' % (lni, lno0, lno1)
#            print '[info] \t\t' + str(params_in[lni][0].shape) + ' -> ' \
#                    + str(net_out.params[lno0][0].data.shape) + ' + ' \
#                    + str(net_out.params[lno1][0].data.shape)
#            print '[info] \t\t' + str(params_in[lni][1].shape) + ' -> ' + str(net_out.params[lno1][1].data.shape)
#            reduced_dim = net_out.params[lno0][0].data.shape[0]
#            print '[info] \t\t SVD reduces dim from ' + str(net_out.params[lno0][0].shape[0]) + ' to ' + str( reduced_dim )
#
#            weight = np.asarray( params_in[lni][0] )
#            bias   = np.asarray( params_in[lni][1] )
#            weight_reshaped = np.transpose( weight.reshape( (weight.shape[0], -1) ) )
#            U,s,V = np.linalg.svd(weight_reshaped, full_matrices=True)
#            print '[info] \t\t' + str(U.shape), str(s.shape), str(V.shape)
#            #print s
#            print '[info] ratio:', np.sum(s[0:reduced_dim]) / np.sum(s)
#            U = U[:,0:reduced_dim]
#            S = np.zeros((reduced_dim, V.shape[0]), dtype=np.float32)
#            S[:reduced_dim, :reduced_dim] = np.diag(s[0:reduced_dim])
#            weight0 = U
#            weight1 = np.dot(S, V)
#            print '[info] \t\t weight shape: ' + str(weight0.shape), str(weight1.shape)
#            #print weight0.dtype
#            #print weight1.dtype
#            reconstructed_weight = np.dot(weight0, weight1)
#            print '[info] \t\tthe maximum value:', np.max(np.abs(weight_reshaped))
#            print '[info] \t\tthe maximum diff:', np.max(np.abs(reconstructed_weight - weight_reshaped))
#            net_out.params[lno0][0].data[...] = np.transpose( weight0 ).reshape( net_out.params[lno0][0].data.shape )
#            net_out.params[lno1][0].data[...] = np.transpose( weight1 ).reshape( net_out.params[lno1][0].data.shape )
#            net_out.params[lno1][1].data[...] = bias
#        
    net_out.save( args.model_out )
##
##    sys.exit(0)
##
##    print 'Blobs: '
##    for b in net.blobs:
##        print b, type( net.blobs[b] )
##
##    print '----------'
##    print 'Params: '
##    for layer in net.params:
##        print layer, type(net.params[layer])
##
##    for s in net.blobs['inception_5b/output'].shape:
##        sys.stdout.write(str(s) + 'x')
##    sys.stdout.write('\n')
##
##
##    sys.stdout.flush()
