import sys
import re
import caffe
import numpy as np
import argparse
from collections import defaultdict
from PIL import Image
import cv2
from matplotlib import pyplot as plt
import math

if __name__ == "__main__":
    floatpat = re.compile('^(\+|-)?\d+(\.\d+)?(e(\+|-)?\d+)?$')
    score = {}
    label = {}
    for idx in range(1, len(sys.argv) ):
        with open( sys.argv[idx], 'r' ) as file:
            for line in file:
                fields = line.split(' ')
                if len( fields ) < 3 or not fields[1].isdigit() or floatpat.match(fields[2]) == None:
                    continue
                if idx == 1:
                    score[fields[0]] = []
                    for findex in range(2, len(fields)):
                        score[fields[0]].append( float( fields[findex] ) )
                        label[fields[0]] = int( fields[1] )
                else:
                    for findex in range(2, len(fields)):
                        score[fields[0]][findex - 2] += float( fields[findex] )

    tot = 0.0
    totcor = 0.0
    for idx, key in enumerate(score):
        sys.stdout.write(key)
        for cls, sc in enumerate(score[key]):
            score[key][cls] /= (len(sys.argv) - 1)
            sys.stdout.write(' %s' % (score[key][cls]) )
        sys.stdout.write('\n')

        tot += 1
        if label[key] == score[key].index(max(score[key])):
            totcor += 1
        if idx % 100 == 0:
            print '[info] processed %d files with precision %f' % (idx, totcor / tot)

    print '[info] processed %d files with precision %f' % (idx, totcor / tot)

    
