#import sys
import caffe
#import matplotlib
import numpy as np
import lmdb
import argparse
#from collections import defaultdict
import cv2
#import pylab as pl
#import os

def rgb_heat(minimum, maximum, value):
    minimum, maximum = float(minimum), float(maximum)
    ratio = 2 * (value-minimum) / (maximum - minimum)
    b = (np.maximum(0, 255*(1 - ratio))).astype(np.uint8)
    r = (np.maximum(0, 255*(ratio - 1))).astype(np.uint8)
    g = 255 - b - r
    return r, g, b

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="draw segmentation result according to caffe feature stored into an lmdb")
    parser.add_argument('--lmdb', type=str, required=True)
    parser.add_argument('--flist', type=str, required=True)
    parser.add_argument('--thresh', type=float, required=False, default=0.5)
    args = parser.parse_args()

    lmdb_env = lmdb.open(args.lmdb)
    lmdb_txn = lmdb_env.begin()
    lmdb_cursor = lmdb_txn.cursor()

    fl = []
    with open(args.flist) as ifile:
        for line in ifile:
            fl.append(line.strip().rstrip().split(' '))

    tot0 = 0.0
    tot1 = 0.0
    totp0 = 0.0
    totp1 = 0.0
    cor0 = 0.0
    cor1 = 0.0

    idx = 0
    for key, value in lmdb_cursor:
        datum = caffe.proto.caffe_pb2.Datum()
        datum.ParseFromString(value)
        label = int(datum.label)
        prob = caffe.io.datum_to_array(datum)
        prob = np.exp(prob[1]) / (np.exp(prob[0]) + np.exp(prob[1]))
        predict = np.zeros(prob.shape, np.uint8)
        predict[ prob > args.thresh ] = 1
        gt = cv2.imread( fl[idx][1] )[:,:,0]

        height = min(gt.shape[0], predict.shape[0])
        width = min(gt.shape[1], predict.shape[1])

        predict = predict[0:height, 0:width]
        gt      = gt[0:height, 0:width]

        gtnz = np.count_nonzero(gt)
        pdnz = np.count_nonzero(predict)

        tot1  += gtnz
        tot0  += gt.shape[0] * gt.shape[1] - gtnz
        totp1 += pdnz
        totp0 += predict.shape[0] * predict.shape[1] - pdnz
        cor1  += np.count_nonzero( np.logical_and(gt, predict) )
        cor0  += gt.shape[0] * gt.shape[1] - np.count_nonzero( np.logical_or(gt, predict) )
        
        #print "\t", tot0, tot1, totp0, totp1, cor0, cor1
        print "%.5f\t%.5f\t%.5f\t%.5f" % ( cor0/totp0, cor0/tot0, cor1/totp1, cor1/tot1 )
        idx = idx + 1

    print "final: p0 r0 p1 r1"
    print cor0/totp0, cor0/tot0, cor1/totp1, cor1/tot1
