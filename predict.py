import cv2
import sys
import traceback
import caffe
import numpy as np
import argparse
from collections import defaultdict
from PIL import Image
from matplotlib import pyplot as plt
import math
from datetime import datetime
import urllib
from pprint import pprint
import json
from shapely.geometry import Polygon
from shapely.geos import TopologicalError
from os import path
import os
import distutils.util

def url_to_image(url):
    # download the image, convert it to a NumPy array, and then read
    # it into OpenCV format
    resp = urllib.urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    return image

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('command', type=str, help='extract | predict')
    parser.add_argument('--nj', type=int, default=1)
    parser.add_argument('--nthj', type=int, default=0)
    parser.add_argument('--proto', type=str, required=True)
    parser.add_argument('--model', type=str, required=True)
    parser.add_argument('--output_blob_name', type=str, required=True)
    parser.add_argument('--apply_softmax', type=bool, default=False)
    parser.add_argument('--list_file', type=str, required=True)
    parser.add_argument('--output_file', type=str, required=True)
    parser.add_argument('--nthfield', type=int, default=0)
    parser.add_argument('--label_map', type=str)
    parser.add_argument('--topn', type=int, default=1)
    parser.add_argument('--gpu_id', type=int, required=False)
    parser.add_argument('--mean0', type=float, default=0.0)
    parser.add_argument('--mean1', type=float, default=0.0)
    parser.add_argument('--mean2', type=float, default=0.0)
    parser.add_argument('--resize_width', type=int, default=0)
    parser.add_argument('--resize_height', type=int, default=0)
    parser.add_argument('--crop_width', type=int, default=0)
    parser.add_argument('--crop_height', type=int, default=0)
    parser.add_argument('--imscale', type=float, default=1.0)

    args = parser.parse_args()

    net = caffe.Net(args.proto, args.model, caffe.TEST)
    if args.gpu_id != None:
        caffe.set_mode_gpu()
        caffe.set_device(args.gpu_id)
    else:
        caffe.set_mode_cpu()

    label_map = []
    if args.label_map is not None and args.label_map != '':
        with open(args.label_map) as f:
            label_map = [cls.replace("\r", "") for cls in f.read().split("\n")]

    print("[info] <%s> : Start ..." % (str(datetime.now())))
    with open(args.list_file, 'r') as listf, open(args.output_file, 'w') as output_file:
        for idx, line in enumerate(listf):
            if idx % args.nj != args.nthj:
                continue
            try:
                fields = line.split()
                if len(fields) <= args.nthfield:
                    sys.stdout.write("[error]: incorrect line: %s with %d fields\n" % (line, len(fields)))
                    continue

                imgloc = fields[args.nthfield]
                if imgloc.startswith( 'http' ):
                    image_orig = url_to_image(imgloc)
                else:
                    image_orig = cv2.imread(imgloc)
                shape_orig = image_orig.shape
                if args.resize_width != 0 and args.resize_height != 0:
                    image_orig = cv2.resize(image_orig, (int(args.resize_width), int(args.resize_height)))

                if args.crop_width != 0 and args.crop_height != 0:
                    if args.crop_width <= image_orig.shape[1] and args.crop_height <= image_orig.shape[0]:
                        oh = (image_orig.shape[0] - args.crop_height) / 2
                        ow = (image_orig.shape[1] - args.crop_width) / 2
                        image_orig = image_orig[oh:oh+args.crop_height, ow:ow+args.crop_width, :]

                image_orig = image_orig.astype(np.float32)
                image = np.copy(image_orig)

                image[:,:,0] = image[:,:,0] - args.mean0
                image[:,:,1] = image[:,:,1] - args.mean1
                image[:,:,2] = image[:,:,2] - args.mean2

                image = np.rollaxis(image, 2)

                input_shape = [ net.blobs[net.inputs[0]].shape[i] for i in range(0, len(net.blobs[ net.inputs[0] ].shape)) ]
                if image.shape[1] != input_shape[2] or image.shape[2] != input_shape[3]:
                    net.blobs[net.inputs[0]].reshape( 1, image.shape[0], image.shape[1], image.shape[2] )

                out = net.forward_all(data=np.asarray([image]))
                feature = np.asarray(out[args.output_blob_name])
                if args.apply_softmax:
                    feature = np.exp(feature[0,1,:,:]) / (np.exp(feature[0,0,:,:]) + np.exp(feature[0,1,:,:]))

                if args.command == 'extract':
                    output_file.write("%s\t%s\n" % (imgloc, " ".join(feature.flatten().tolist())))
                if args.command == 'predict':
                    label_str = "\t".join([label_map[idx] for idx in np.argsort(feature.flatten())[::-1][0:args.topn].tolist()])
                    output_file.write("%s\t%s\n" % (imgloc, label_str))
            except Exception as e:
                print(e)


