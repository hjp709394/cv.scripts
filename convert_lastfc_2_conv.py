import sys
from pprint import pprint
import traceback
import caffe
import numpy as np
import argparse
from collections import defaultdict
from PIL import Image
import cv2
from matplotlib import pyplot as plt
import math

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--proto_in', type=str, required=True)
    parser.add_argument('--proto_out', type=str, required=True )
    parser.add_argument('--model_in', type=str, required=True)
    parser.add_argument('--model_out', type=str, required=True)
    parser.add_argument('--layer_name_in', nargs='*', type=str, required=False, help="Do not need to use this option if the layer name for model_in and model_out is the same")
    parser.add_argument('--layer_name_out', nargs='*', type=str, required=False)
    args = parser.parse_args()

    if args.layer_name_in is not None and args.layer_name_out is None or args.layer_name_in is None and args.layer_name_out is not None or args.layer_name_in is not None and args.layer_name_out is not None and len( args.layer_name_in ) != len( args.layer_name_out ):
        print "[error] number of layer_name_in does not match with that of layer_name_out ( %d vs %d )" % ( len(args.layer_name_in), len(args.layer_name_out) )
        sys.exit(1)

    print '[info] loading net_in ' + args.proto_in
    net_in  = caffe.Net(args.proto_in , args.model_in , caffe.TEST)
    print '[info] loading net_in ' + args.proto_out
    #net_out = caffe.Net(args.proto_out, args.model_in, caffe.TEST)
    net_out = caffe.Net(args.proto_out, caffe.TEST)

    for key in net_out.params:
        if key in net_in.params:
            print '[info] model_in param          exist for layer: ' + key
        else:
            print '[info] model_in param does not exist for layer: ' + key

    print '[info] copying layers parameters'
    for key in net_out.params:
        lno = lni = key
        if key not in net_in.params:
            continue
        print '[info] %s -> %s' % (lni, lno)
        print '[info] \t\t' + str(net_in.params[lni][0].data.shape) + ' -> ' + str(net_out.params[lno][0].data.shape)
        print '[info] \t\t' + str(net_in.params[lni][1].data.shape) + ' -> ' + str(net_out.params[lno][1].data.shape)
        net_out.params[lno][0].data[...] = net_in.params[lni][0].data.reshape( net_out.params[lno][0].data.shape )
        net_out.params[lno][1].data[...] = net_in.params[lni][1].data.reshape( net_out.params[lno][1].data.shape )

    if args.layer_name_in is not None and args.layer_name_out is not None:
        print '[info] converting layers parameters : ' 
        for idx, lni in enumerate( args.layer_name_in ):
            lno = args.layer_name_out[idx]
            print '[info] %s -> %s' % (lni, lno)
            print '[info] \t\t' + str(net_in.params[lni][0].data.shape) + ' -> ' + str(net_out.params[lno][0].data.shape)
            print '[info] \t\t' + str(net_in.params[lni][1].data.shape) + ' -> ' + str(net_out.params[lno][1].data.shape)
            net_out.params[lno][0].data[...] = net_in.params[lni][0].data.reshape( net_out.params[lno][0].data.shape )
            net_out.params[lno][1].data[...] = net_in.params[lni][1].data.reshape( net_out.params[lno][1].data.shape )
        
    net_out.save( args.model_out )

    sys.exit(0)

    print 'Blobs: '
    for b in net.blobs:
        print b, type( net.blobs[b] )

    print '----------'
    print 'Params: '
    for layer in net.params:
        print layer, type(net.params[layer])

    for s in net.blobs['inception_5b/output'].shape:
        sys.stdout.write(str(s) + 'x')
    sys.stdout.write('\n')


    sys.stdout.flush()
