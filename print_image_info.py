import cStringIO
import imghdr
import urllib2
import sys
import traceback
import numpy as np
import argparse
from collections import defaultdict
from PIL import Image
import cv2
from matplotlib import pyplot as plt
import math
from datetime import datetime
import urllib

def url_to_image(url):
    # download the image, convert it to a NumPy array, and then read
    # it into OpenCV format
    resp = urllib.urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)

    # return the image
    return image

def show_img(img):
    plt.imshow(cv2.cvtColor(img, cv2.COLOR_BGR2RGB));
    plt.show();

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--nj', type=int, default=1)
    parser.add_argument('--nthj', type=int, default=0)
    parser.add_argument('--list_file', type=str, required=True)
    args = parser.parse_args()

    with open(args.list_file, 'r') as listf:
        for idx, line in enumerate(listf):
            line = line.strip().rstrip()
            if idx % args.nj != args.nthj:
                continue

            fields = line.strip().rstrip().split()
            file = fields[0]

            try:
                if file.startswith( 'http' ):
                    response = urllib2.urlopen(file)
                    data = cStringIO.StringIO(response.read())
                else:
                    with open(file, 'r') as f:
                        data = cStringIO.StringIO(f.read())
                img_type = imghdr.what(data)
                data.seek(0)
                img_array = np.asarray(bytearray(data.read()), dtype=np.uint8)
                img = cv2.imdecode(img_array, -1)
                print '[info]', file, img_type, img.shape
            except:
                print '[eror]', file


