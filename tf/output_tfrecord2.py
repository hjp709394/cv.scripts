import PIL.Image
import tensorflow as tf
import numpy as np
import io
import cv2
parallel_reader = tf.contrib.slim.parallel_reader

flags = tf.app.flags
flags.DEFINE_string('tfrecord', '', 'Path to tfrecord')
FLAGS = flags.FLAGS

def read_and_decode(filename):
    _, string_tensor = parallel_reader.parallel_read(
     filename,
     reader_class=tf.TFRecordReader,
     num_epochs=1,
     num_readers=8,
     shuffle=False,
     dtypes=[tf.string, tf.string],
     capacity=2000,
     min_after_dequeue=1000)
    sv = tf.train.Supervisor('/temp/debug_tfrecord')
    with sv.prepare_or_wait_for_session() as sess:
      sv.start_queue_runners(sess)
      example = sess.run([string_tensor])


#    record_iterator = tf.python_io.tf_record_iterator(path=filename)
#    for string_record in record_iterator:
#    	example = tf.train.Example()
#	example.ParseFromString(string_record)
#	height = int(example.features.feature['image/height'].int64_list.value[0])
#	width = int(example.features.feature['image/width'].int64_list.value[0])
#	classes_text = example.features.feature['image/object/class/text'].bytes_list.value
#	bbox = example.features.feature['image/object/bbox/xmin'].float_list.value
#        encoded_jpg = example.features.feature['image/encoded'].bytes_list.value[0]
#	imgfn = example.features.feature['image/filename'].bytes_list.value
#        print('imgfn:', imgfn[0])
#	for i in range( len(classes_text) ):
#	    print(classes_text[i])
#	    if classes_text[i] == 'toaster':
#		nparr = np.fromstring(encoded_jpg, np.uint8)
#		img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
#	    	print('matched', len(bbox), img.shape)
#      'image/height': dataset_util.int64_feature(height),
#      'image/width': dataset_util.int64_feature(width),
#      'image/filename': dataset_util.bytes_feature(
#          img_file_name.encode('utf8')),
#      'image/source_id': dataset_util.bytes_feature(
#          img_file_name.encode('utf8')),
#      'image/key/sha256': dataset_util.bytes_feature(key.encode('utf8')),
#      'image/encoded': dataset_util.bytes_feature(encoded_jpg),
#      'image/format': dataset_util.bytes_feature('jpeg'.encode('utf8')),
#      'image/object/bbox/xmin': dataset_util.float_list_feature(xmin),
#      'image/object/bbox/xmax': dataset_util.float_list_feature(xmax),
#      'image/object/bbox/ymin': dataset_util.float_list_feature(ymin),
#      'image/object/bbox/ymax': dataset_util.float_list_feature(ymax),
#      'image/object/class/text': dataset_util.bytes_list_feature(classes_text),
#      'image/object/class/label': dataset_util.int64_list_feature(classes),
#      'image/object/difficult': dataset_util.int64_list_feature(difficult_obj),
#      'image/object/truncated': dataset_util.int64_list_feature(truncated),
#      'image/object/view': dataset_util.bytes_list_feature(poses),

if __name__ == '__main__':
    read_and_decode(FLAGS.tfrecord)

