import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image
import json
import cv2

flags = tf.app.flags
flags.DEFINE_string('pb', '', 'Path to pb model file')
flags.DEFINE_string('info', '', 'Path to info json file')
flags.DEFINE_string('image_folder', '', 'Path to test image folder')
flags.DEFINE_string('output', '', 'Path to output json file')
flags.DEFINE_string('label_map', '', 'Path to label_map file')
FLAGS = flags.FLAGS

from utils import label_map_util
from utils import visualization_utils as vis_util

def main(_):
    PATH_TO_CKPT = FLAGS.pb

    # List of the strings that is used to add correct label for each box.
    PATH_TO_LABELS = FLAGS.label_map

    PATH_TO_INFO = FLAGS.info
    with open(PATH_TO_INFO) as f:
        info = json.loads(f.read())
        print(info['images'][0], len(info['images']))
        print(info['categories'][0])
        print(info['licenses'])
        print(info['info'])
        for key in info:
            print(key)

    IMAGE_FOLDER = FLAGS.image_folder

    label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
    print label_map.item[0].id
    NUM_CLASSES = len(label_map.item)

    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')

    def load_image_into_numpy_array(image):
      (im_width, im_height) = image.size
      return np.array(image.getdata()).reshape(
          (im_height, im_width, 3)).astype(np.uint8)

    result = list()
    with detection_graph.as_default():
      with tf.Session(graph=detection_graph) as sess:
        for iIdx, image_info in enumerate(info['images']):
          #print(IMAGE_FOLDER, image_info)
          #print(os.path.join(IMAGE_FOLDER, image_info['file_name']))
          image = Image.open(os.path.join(IMAGE_FOLDER, image_info['file_name']))
          # the array based representation of the image will be used later in order to prepare the
          # result image with boxes and labels on it.
          image_np = load_image_into_numpy_array(image)
          image_height, image_width, _ = image_np.shape
          #print(image_np.shape)
          # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
          image_np_expanded = np.expand_dims(image_np, axis=0)
          image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
          # Each box represents a part of the image where a particular object was detected.
          boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
          # Each score represent how level of confidence for each of the objects.
          # Score is shown on the result image, together with the class label.
          scores = detection_graph.get_tensor_by_name('detection_scores:0')
          classes = detection_graph.get_tensor_by_name('detection_classes:0')
          num_detections = detection_graph.get_tensor_by_name('num_detections:0')
          # Actual detection.
          (boxes, scores, classes, num_detections) = sess.run(
              [boxes, scores, classes, num_detections],
              feed_dict={image_tensor: image_np_expanded})

          for bIdx, box in enumerate(boxes[0]):
              ymin, xmin, ymax, xmax = box.tolist()
              x = int(xmin * image_width)
              y = int(ymin * image_height)
              width = int((xmax - xmin) * image_width)
              height = int((ymax-ymin) * image_height)

              image_id = image_info['id']

              label = int(classes[0][bIdx] + 0.5)
              category_id = label_map.item[label-1].id

              score = float(scores[0][bIdx])
              result.append({'image_id': image_id, 'category_id': category_id, 'bbox': [x, y, width, height], 'score': score})
          if iIdx % 100 == 0:
              print('%d / %d' % (iIdx, len(info['images'])))

    with open(FLAGS.output, 'w') as f:
        json.dump(result, f)

if __name__ == '__main__':
    tf.app.run()
