import json
import os
import glob

fnlist = sorted(glob.glob('./perf/*.json'))
for fn in fnlist:
    with open(fn) as f:
        data = json.loads(f.read())
    dur = 0.
    totdur = 0.
    for item in data['traceEvents']:
        if not 'dur' in item:
            continue
        name = item['args']['name']
        nameArray = name.split('/')
        totdur += item['dur']
        if name.startswith('FeatureExtractor') and not nameArray[2].startswith('Conv2d_13'):
            dur += item['dur']
            #print(item['args']['name'])
    print fn,dur,totdur,dur/totdur

