import tensorflow as tf
from nets.mobilenet_v1 import mobilenet_v1
from tensorflow.python.training import saver as saver_lib
from tensorflow.python import pywrap_tensorflow
from nets import nets_factory
slim = tf.contrib.slim

tf.app.flags.DEFINE_string(
    'checkpoint', '', 'The path to checkpoint')
tf.app.flags.DEFINE_string(
    'output_path', '', 'The path for storing output model')
FLAGS = tf.app.flags.FLAGS

input_checkpoint = FLAGS.checkpoint

# Where to save the modified graph
save_path = FLAGS.output_path

# TODO(shizehao): use graph editor library insead
with tf.Graph().as_default() as graph:
  input_images = tf.placeholder(tf.float32, [None, 224, 224, 3], 'MobileNet/input_images')
  network_fn = nets_factory.get_network_fn(
        'mobilenet_v1',
        num_classes=1001,
        is_training=False)
  logits, end_points = network_fn(input_images)

  #variables_to_restore = tf.get_collection(tf.GraphKeys.VARIABLES)
  variables_to_restore = slim.get_variables_to_restore()
  for key in variables_to_restore:
      print('Restore: ', key)
  saver = tf.train.Saver()

  if tf.gfile.IsDirectory(FLAGS.checkpoint):
    checkpoint_path = tf.train.latest_checkpoint(FLAGS.checkpoint)
  else:
    checkpoint_path = FLAGS.checkpoint
  with tf.Session() as sess:
#    var_list = {}
#    reader = pywrap_tensorflow.NewCheckpointReader(input_checkpoint)
#    var_to_shape_map = reader.get_variable_to_shape_map()
#    for key in var_to_shape_map:
#      try:
#        tensor = sess.graph.get_tensor_by_name(key + ":0")
#      except KeyError:
#        # This tensor doesn't exist in the graph (for example it's
#        # 'global_step' or a similar housekeeping element) so skip it.
#        continue
#      print('Save ', key)
#      var_list[key] = tensor
#    saver = saver_lib.Saver(var_list=var_list)

    # Restore variables
    saver.restore(sess, input_checkpoint)

    # Save new checkpoint and the graph
    saver.save(sess, save_path + '/model')
    tf.train.write_graph(graph, save_path, 'graph.pbtxt')

  print ('[INFO] Input node name: ', input_images.name)
  print ('[INFO] Output node name: ', end_points['Predictions'].name)
