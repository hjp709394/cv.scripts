#!/home/huangjiapei/anaconda2/bin/python

from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
#from sqlalchemy.orm import relation, sessionmaker

Base = declarative_base()

class Entry(Base):

    __tablename__ = 'entries'

    volume_id = Column(Integer, primary_key=True)
    entry_id = Column(Integer, primary_key=True)
    value1 = Column(Integer)
    moderator1_id = Column(Integer)
    value2 = Column(Integer)
    moderator2_id = Column(Integer)
    thumbnail_url = Column(String)
    original_url = Column(String)
    updated_at = Column(DateTime)
    flag = Column(Integer)

    def __init__(self, **params):
        self.volume_id = params['volume_id']
        self.entry_id = params['entry_id']
        self.value1 = params['value1']
        self.moderator1_id = params['moderator1_id']
        self.value2 = params['value2']
        self.moderator2_id = params['moderator2_id']
        self.thumbnail_url = params['thumbnail_url']
        self.original_url = params['original_url']
        self.flag = params['flag']

#    def __repr__(self):
#        return "entries(%d, %d, %d, %d, %d, %d, %s, %s, %d)" % (self.volume_id, self.entry_id, self.value1, self.moderator1_id, self.value2, self.moderator2_id, self.thumbnail_url, self.original_url, self.flag);

    def __json__(self):
        return {
            "volume_id" : self.volume_id,
            "entry_id"  : self.entry_id,
            "thumbnail_url": self.thumbnail_url,
            "original_url" : self.original_url,
            "value1"    : self.value1,
            "moderator1_id": self.moderator1_id,
            "updated_at": str(self.updated_at)
        }


